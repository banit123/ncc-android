package com.agero.ncc.activities;


import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.JobIntentService;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.agero.ncc.BuildConfig;
import com.agero.ncc.R;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.db.database.NccDatabase;
import com.agero.ncc.driver.fragments.ActiveJobsFragment;
import com.agero.ncc.firebasefunctions.FirebaseFunctions;
import com.agero.ncc.firebasefunctions.FirebaseFunctionsException;
import com.agero.ncc.firebasefunctions.HttpsCallableResult;
import com.agero.ncc.forceupdate.ForceUpdateChecker;
import com.agero.ncc.fragments.AccountFragment;
import com.agero.ncc.fragments.AlertFragment;
import com.agero.ncc.fragments.ChatHistoryFragment;
import com.agero.ncc.fragments.DateListener;
import com.agero.ncc.fragments.EquipmentSelectionFragment;
import com.agero.ncc.fragments.HistoryFragment;
import com.agero.ncc.fragments.PermissionFragment;
import com.agero.ncc.fragments.TermsAndConditionsFragment;
import com.agero.ncc.model.Equipment;
import com.agero.ncc.model.SparkLocationData;
import com.agero.ncc.model.TermsAndConditionsAcceptenceModel;
import com.agero.ncc.model.TokenModel;
import com.agero.ncc.retrofit.NccApi;
import com.agero.ncc.services.NCCBadgeUpdateService;
import com.agero.ncc.services.SparkLocationUpdateService;
import com.agero.ncc.utils.DateTimeUtils;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UiUtils;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.agero.ncc.views.NccBottomNavigationView;
import com.agero.nccsdk.NccSdk;
import com.agero.nccsdk.NccSdkInterface;
import com.agero.nccsdk.NccSensorListener;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.splunk.mint.Mint;
import com.splunk.mint.MintLogLevel;
import com.vicmikhailau.maskededittext.MaskedFormatter;
import com.vicmikhailau.maskededittext.MaskedWatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.chatsdk.core.base.BaseHookHandler;
import co.chatsdk.core.dao.Thread;
import co.chatsdk.core.events.NetworkEvent;
import co.chatsdk.core.interfaces.ThreadType;
import co.chatsdk.core.session.ChatSDK;
import co.chatsdk.core.session.NM;
import co.chatsdk.core.session.NetworkManager;
import co.chatsdk.core.session.StorageManager;
import co.chatsdk.core.types.AuthKeys;
import co.chatsdk.firebase.FirebaseEventHandler;
import co.chatsdk.firebase.wrappers.UserWrapper;
import co.chatsdk.ui.manager.InterfaceManager;
import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import q.rorbin.badgeview.QBadgeView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.agero.ncc.utils.LogUtilsKt.debug;

public class HomeActivity extends BaseActivity implements ForceUpdateChecker.OnUpdateNeededListener {

    private static boolean mIsInitialised = false, mIsRegistered = false;
    @BindView(R.id.bar_bottom)
    public NccBottomNavigationView mBottomBar;
    protected PublishSubject<Location> mLocationSubject = PublishSubject.create();
    protected boolean isPermissionDenied;
    @BindView(R.id.constarint_activejobs)
    ConstraintLayout mConstarintEquipment;
    @BindView(R.id.relative_duty)
    RelativeLayout mRelativeDuty;
    @BindView(R.id.relative_spinner_duty)
    RelativeLayout mRelativeSpinnerDuty;
    @BindView(R.id.text_duty_myjobs)
    TextView mTextDutyMyJobs;
    @BindView(R.id.text_duty)
    TextView mTextDuty;
    @BindView(R.id.relative_jobdetail)
    RelativeLayout mRelativeJobdetail;
    @BindView(R.id.toolbar_jobdetail_text)
    TextView mToolbarJobdetailText;
    @BindView(R.id.imageview_back_arrow)
    ImageView mImageBackArrow;
    @BindView(R.id.imageView_duty_close)
    ImageView mImageViewDutyClose;
    @BindView(R.id.image_save)
    ImageView mImageSave;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.text_active_duty)
    TextView mTextActiveDuty;
    @BindView(R.id.text_active_truck)
    TextView mTextActiveTruck;
    @BindView(R.id.text_change)
    TextView textChange;
    @BindView(R.id.frame_home_screen_container)
    FrameLayout frameHomeScreenContainer;
    ToolbarSaveListener mToolbarSaveListener;
    ToolbarSaveCancelListener mToolbarSaveCancelListener;
    JobDetailToolbarLisener mJobDetailToolbarLisener;
    @BindView(R.id.image_notes)
    ImageView mImageNotes;
    @BindView(R.id.badge_target_large)
    View mImageChatBadgeTargetLarge;
    @BindView(R.id.badge_target)
    View mImageChatBadgeTarget;
    @BindView(R.id.image_call)
    ImageView mImageCall;
    @BindView(R.id.constraint_duty)
    ConstraintLayout mConstraintDuty;
    @BindView(R.id.text_next)
    TextView mTextNext;
    @BindView(R.id.text_done)
    TextView mTextDone;
    @BindView(R.id.image_dispatcher_options)
    ImageView mImageDispatcherOptions;
    @BindView(R.id.image_notes_large)
    ImageView mImageNotesLarge;
    @BindView(R.id.image_call_large)
    ImageView mImageCallLarge;
    @BindView(R.id.image_dispatcher_options_large)
    ImageView mImageDispatcherOptionsLarge;
    @BindView(R.id.image_search_icon_large)
    ImageView mImageSearchIconLarge;
    //@BindView(R.id.relative_spinner_duty)
    //@BindView(R.id.relative_spinner_duty)
    //RelativeLayout mRelativeSpinnerDuty;
    @BindView(R.id.text_duty_large)
    TextView mTextDutyLarge;
    @BindView(R.id.toolbar_jobdetail_text_history)
    TextView mTextHistoryLarge;
    @BindView(R.id.relative_jobdetail_large)
    RelativeLayout mRelativeJobdetailLarge;
    AlertDetailToolbarLisener mAlertDetailToolbarLisener;
    private boolean isPermissionScreenClosed = false;
    UserError mUserError;
    @Inject
    SharedPreferences mPrefs;
    @Inject
    NccDatabase nccDatabase;
    @Inject
    NccApi nccApi;

    PermissionListener phoneCallPermissionListener = new PermissionListener() {
        @Override
        public void onPermissionGranted(PermissionGrantedResponse response) {
            String phoneNumber = mPrefs.getString(NccConstants.DISPATCHER_NUMBER, "");
            if (!TextUtils.isEmpty(phoneNumber)) {
                Utils.makePhoneCall(HomeActivity.this, phoneNumber);
            }
        }

        @Override
        public void onPermissionDenied(PermissionDeniedResponse response) {
            if (response.isPermanentlyDenied()) {
                isPermissionDenied = true;
            }
        }

        @Override
        public void onPermissionRationaleShouldBeShown(PermissionRequest permission,
                                                       PermissionToken token) {
            token.continuePermissionRequest();
        }
    };

    //    private String previousSelected = "Jobs";
    private String eventAction = "";
    private String eventCategory = NccConstants.FirebaseEventCategory.JOBS;
    BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            disableSearchListener();
            clearDate();
            clearSearch();

            if (mConstraintDuty.getVisibility() == View.GONE) {
                int id = item.getItemId();
                Bundle bundle = new Bundle();
                String navigationItemClicked = "";
                String currentScreen = eventCategory;
                switch (id) {
                    case R.id.action_active:
                        if (isLoggedInDriver()) {
                            Timber.d("loading Driver Screen");
                            navigationItemClicked = NccConstants.FirebaseEventAction.VIEW_JOBS;
                            push(ActiveJobsFragment.newInstance());
                        } else {
                            Timber.d("loading Dispatcher Screen");
                            navigationItemClicked = NccConstants.FirebaseEventAction.VIEW_JOBS;
                            push(com.agero.ncc.dispatcher.fragments.ActiveJobsFragment.newInstance());
                        }
                        eventCategory = NccConstants.FirebaseEventCategory.JOBS;
                        bottomBarSelected = 0;
                        break;
                    case R.id.action_notifications:
                        isPermissionScreenClosed = true;
                        navigationItemClicked = NccConstants.FirebaseEventAction.VIEW_ALERTS;
                        push(AlertFragment.newInstance());
                        eventCategory = NccConstants.FirebaseEventCategory.ALERTS;
                        bottomBarSelected = 3;
                        break;
                    case R.id.action_history:
                        isPermissionScreenClosed = true;
                        navigationItemClicked = NccConstants.FirebaseEventAction.VIEW_HISTORY;
                        push(HistoryFragment.newInstance(true, mJobId));
                        eventCategory = NccConstants.FirebaseEventCategory.HISTORY;
                        bottomBarSelected = 1;
                        break;
                    case R.id.action_account:
                        isPermissionScreenClosed = true;
                        navigationItemClicked = NccConstants.FirebaseEventAction.VIEW_ACCOUNT;
                        push(AccountFragment.newInstance());
                        eventCategory = NccConstants.FirebaseEventCategory.ACCOUNT;
                        bottomBarSelected = 4;
                        break;
                    case R.id.action_chat:
                        isPermissionScreenClosed = true;
                        navigationItemClicked = NccConstants.FirebaseEventAction.CHAT_LIST;
                        push(ChatHistoryFragment.newInstance());
                        eventCategory = NccConstants.FirebaseEventCategory.CHAT;
                        bottomBarSelected = 2;
                        break;
                }
                bundle.putString(NccConstants.FirebaseLogKey.KEY_BOTTOM_BAR, navigationItemClicked);
//                getMFirebaseAnalytics().logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
                firebaseLogEvent(currentScreen, navigationItemClicked);

                return true;
            } else {
                return false;
            }

        }
    };
    private String fcmId, fcmCurrentToken;
    private SharedPreferences.Editor mEditor;
    private QBadgeView qBadgeView, chatBadgeView, alertBadgeView, jobChatBadgeView;
    private LocationManager locationManager;
    private String provider;
    private Location location;
    private DatabaseReference myRef;
    private DatabaseReference myLocationRef;
    private Switch mDutySwitch;
    private TextView mToolbarText;
    private OnDutyToolbarListener onDutyToolbarListener;
    private int serviceUnFinished = 0;
    private NccSensorListener singleListener;
    private BroadcastReceiver mAlertBroadCastReceiver, mChatBroadCastReceiver;

    private View searchLayoutLarge;
    private TextView datePickerLarge;

    private View searchLayout;
    private TextView datePicker;
    private SimpleDateFormat dateFormatter = new SimpleDateFormat("EEE, MMM dd", Locale.US);
    private SearchView searchView;
    private SearchView searchViewLarge;

    private ImageView clearDate;
    private ImageView clearDateLarge;
    private DateListener dateListener;

    private AutoCompleteTextView searchEditText;
    private AutoCompleteTextView searchEditTextLarge;
    private View searchClear, searchClearLarge, searchClose, searchCloseLarge, searchSaveLarge, searchSave;
    private Calendar selectedDate;
    MaskedFormatter formatter;
    private boolean isFromChat = false;
    private String mJobId;
    private boolean mIsJobCompleted;
    private FusedLocationProviderClient mFusedLocationClient;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NCCApplication.getContext().getComponent().inject(this);
        setContentView(R.layout.activity_home);
        NCCBadgeUpdateService.mOldBadgeCount = 0;
        ButterKnife.bind(this);
//        NCCBadgeUpdateService.mIsFirstTime = true;
        mEditor = mPrefs.edit();
        if (!mPrefs.contains(NccConstants.SIGNIN_USER_ID) && TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""))) {
            Intent intent = new Intent(HomeActivity.this, WelcomeActivity.class);
            clearSigninData();
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
            return;
        }
        if (getIntent() != null) {
            isFromChat = getIntent().getBooleanExtra(NccConstants.IS_FROM_CHAT, false);
            mJobId = getIntent().getStringExtra(NccConstants.JOB_ID);
            mIsJobCompleted = getIntent().getBooleanExtra(NccConstants.IS_JOB_COMPLETED, false);
        }


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mDutySwitch = (Switch) findViewById(R.id.switch_duty);
        mToolbarText = (TextView) toolbar.findViewById(R.id.toolbar_text);
        searchLayout = findViewById(R.id.searchLayout);
        InputFilter[] fArray = new InputFilter[1];
        fArray[0] = new InputFilter.LengthFilter(15);
        //search layout customs
        if (searchLayout != null) {
            searchEditText = searchLayout.findViewById(android.support.v7.appcompat.R.id.search_src_text);
            searchEditText.setFilters(fArray);
            formatter = new MaskedFormatter("###-###-###-###");
            searchEditText.addTextChangedListener(new MaskedWatcher(formatter, searchEditText));
            searchEditText.setTextColor(getResources().getColor(R.color.search_text_color_black));
            searchEditText.setHint(getString(R.string.any_job_id));
            searchEditText.setTextSize(16);
            searchEditText.setHintTextColor(getResources().getColor(R.color.ncc_hint_color));
            searchEditText.setBackground(null);
            searchEditText.setBackgroundColor(Color.TRANSPARENT);
            searchEditText.setFocusable(false);
            searchEditText.setFocusableInTouchMode(false);
            changeSearchCursorColor(searchEditText);
        }
        datePicker = findViewById(R.id.datePicker);
        searchView = findViewById(R.id.tvSearchKey);
        if (searchView != null) {
            searchClear = searchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
            searchView.findViewById(android.support.v7.appcompat.R.id.search_plate).setBackgroundColor(Color.TRANSPARENT);
        }
        clearDate = findViewById(R.id.clearDate);
        searchClose = findViewById(R.id.close_search);
        searchCloseLarge = findViewById(R.id.close_search_large);
        searchSave = findViewById(R.id.search_icon);
        searchSaveLarge = findViewById(R.id.search_icon_large);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int value = displayMetrics.widthPixels;
        if (displayMetrics.heightPixels > value) {
            value = displayMetrics.heightPixels;
        }

        searchLayoutLarge = findViewById(R.id.searchLayoutLarge);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) searchLayoutLarge.getLayoutParams();
        params.width = (value * 40) / 100;
        searchLayoutLarge.setLayoutParams(params);
        //search layout customs
        if (searchLayoutLarge != null) {
            searchEditTextLarge = searchLayoutLarge.findViewById(android.support.v7.appcompat.R.id.search_src_text);

            searchEditTextLarge.addTextChangedListener(new MaskedWatcher(formatter, searchEditTextLarge));
            searchEditTextLarge.setFilters(fArray);
            searchEditTextLarge.setTextColor(getResources().getColor(R.color.ncc_black));
            searchEditTextLarge.setHint(getString(R.string.any_job_id));
            searchEditTextLarge.setTextSize(16);
            searchEditTextLarge.setHintTextColor(getResources().getColor(R.color.search_hint_text_color));
            searchEditTextLarge.setBackground(null);
            searchEditTextLarge.setBackgroundColor(Color.TRANSPARENT);
            searchEditTextLarge.setFocusable(false);
            searchEditTextLarge.setFocusableInTouchMode(false);
            changeSearchCursorColor(searchEditTextLarge);
        }
        datePickerLarge = findViewById(R.id.datePickerLarge);
        searchViewLarge = findViewById(R.id.tvSearchKeyLarge);

        if (searchViewLarge != null) {
            searchClearLarge = searchViewLarge.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
            searchViewLarge.findViewById(android.support.v7.appcompat.R.id.search_plate).setBackgroundColor(Color.TRANSPARENT);
        }

        clearDateLarge = findViewById(R.id.clearDateLarge);

        mUserError = new UserError();
        if (qBadgeView == null) {
            qBadgeView = new QBadgeView(this);
        }
        if (alertBadgeView == null) {
            alertBadgeView = new QBadgeView(this);
        }
        if (chatBadgeView == null) {
            chatBadgeView = new QBadgeView(this);
        }
        if (jobChatBadgeView == null) {
            jobChatBadgeView = new QBadgeView(this);
        }

        if (savedInstanceState != null) {
            mBottomBar.setVisibility(View.GONE);
            mConstraintDuty.setVisibility(View.GONE);
            hideProgress();
            isPermissionScreenClosed = savedInstanceState.getBoolean(NccConstants.KEY_FROM_SCREEN);
            if (isPermissionScreenClosed) {
                bottomBarSelected = savedInstanceState.getInt(NccConstants.BOTTOMBARSELECTED);
                mBottomBar.setCurrentItem(bottomBarSelected);
            } else {
                checkTermsAndConditions();
            }
        } else {
            if (!TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""))) {
                isPermissionScreenClosed = false;
                mConstraintDuty.setVisibility(View.GONE);
                hideToolBar();
                hideBottomBar();
                checkTermsAndConditions();
            } else {
                Intent intent = new Intent(this, WelcomeActivity.class);
                startActivity(intent);
                finish();
            }


        }

        if (NccConstants.IS_CHAT_ENABLED) {
            if (!mIsInitialised) {
                mIsInitialised = true;
                NCCApplication.initialiseChatModule(HomeActivity.this, mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""));
            }
            if (!mIsRegistered) {
                mIsRegistered = true;
                registerUserForChat();
            }
        } else {
            mBottomBar.getMenu().removeItem(R.id.action_chat);
        }

        mTextDuty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDuty(mTextDuty.getText().toString().trim());
                mConstraintDuty.setVisibility(View.VISIBLE);
                setArrowDropDownIcon(R.drawable.ic_arrow_drop_up_white_24dp);
                setBGClickableFalse();
            }
        });

        mTextDutyLarge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDuty(mTextDutyLarge.getText().toString().trim());
                mConstraintDuty.setVisibility(View.VISIBLE);
                setArrowDropDownIcon(R.drawable.ic_arrow_drop_up_white_24dp);
                setBGClickableFalse();
            }
        });
        mEditor.putBoolean(NccConstants.SHOW_ON_OFF_DUTY, false).commit();
        mTextDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setArrowDropDownIcon(R.drawable.ic_arrow_drop_down_white_24px);
                onDutyUploadComplete();

            }
        });

        textChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                eventAction = NccConstants.FirebaseEventAction.CHANGE_EQUIPMENT;
                firebaseLogEvent(eventCategory, eventAction);
                push(EquipmentSelectionFragment.newInstance(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""), true), getString(R.string.title_select_equipment));
            }
        });

        registerFCM();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference("Users/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "")
                + "/" + mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
        //myRef.keepSynced(true);

        myLocationRef = database.getReference("Locations/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "")
                + "/" + mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
        //myLocationRef.keepSynced(true);

        mFirebaseAnalytics.setUserProperty("UserId", mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
        Crashlytics.setUserIdentifier(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
        Crashlytics.setString("FacilityId", mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""));
        mFirebaseAnalytics.setUserProperty("FacilityId", mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""));

        if (!TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""))) {
            NccSdk.getNccSdk(NCCApplication.getContext()).setUserId(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
        }
        startSparkLocationService();
        ForceUpdateChecker.with(this).onUpdateNeeded(this).check();

        addRoleChangeListener();
        addStatusChangeListener();

        initBadgeView();
        addIgnoringBatteryOptimizationsCallback();

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);


        if (getIntent().hasExtra(NccConstants.NOTIFICATION_TYPE)) {
            if (NccConstants.NOTIFICATION_TYPE_CHAT.equalsIgnoreCase(getIntent().getStringExtra(NccConstants.NOTIFICATION_TYPE))) {
                String threadEntityID = getIntent().getStringExtra(NccConstants.NOTIFICATION_ENTITY_ID);
                ChatActivity.toUserName = "";
                ChatActivity.isFromJobDetail = false;
                InterfaceManager.shared().a.startChatActivityForID(this, threadEntityID);
            }
        }
    }

    private void changeSearchCursorColor(AutoCompleteTextView searchText) {
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchText, R.drawable.cursor); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showDatePicker() {

        UiUtils.hideKeyboard(this);

//        datePickerLarge.setEnabled(false);
//        datePicker.setEnabled(false);

        Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog picker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.clear();
                newDate.set(year, monthOfYear, dayOfMonth);
                selectedDate = newDate;
//                if (getResources().getBoolean(R.bool.isTablet)) {
                datePickerLarge.setEnabled(true);
                datePickerLarge.setText(dateFormatter.format(newDate.getTime()));
                datePickerLarge.setTextColor(getResources().getColor(R.color.search_text_color));
                if (searchEditTextLarge.getHint().toString().equals(getString(R.string.any_job_id)) || searchEditTextLarge.getHint().toString().equals(getString(R.string.formated_zero))) {
                    searchEditTextLarge.setHint(getString(R.string.any_job_id));
                    searchEditTextLarge.setHintTextColor(getResources().getColor(R.color.search_hint_text_color));
                    searchEditText.clearFocus();
                } else {
                    searchEditTextLarge.setHintTextColor(getResources().getColor(R.color.search_text_color));
                }
                clearDateLarge.setVisibility(View.VISIBLE);
                //searchViewLarge.setQuery("", false);
                //searchViewLarge.clearFocus();
//                } else {
                datePicker.setEnabled(true);
                datePicker.setText(dateFormatter.format(newDate.getTime()));
                datePicker.setTextColor(getResources().getColor(R.color.search_text_color));
                if (searchEditText.getHint().toString().equals(getString(R.string.any_job_id)) || searchEditText.getHint().toString().equals(getString(R.string.formated_zero))) {
                    searchEditText.setHint(getString(R.string.any_job_id));
                    searchEditText.setHintTextColor(getResources().getColor(R.color.ncc_hint_color));
                    searchEditText.clearFocus();
                } else {
                    searchEditText.setHintTextColor(getResources().getColor(R.color.ncc_hint_color));
                }
                clearDate.setVisibility(View.VISIBLE);
                //searchView.setQuery("", false);
                //searchView.clearFocus();
//                }

                //clearSearch();
                dateListener.onDateSet(newDate.getTime());
            }

        },
                selectedDate != null ? selectedDate.get(Calendar.YEAR) : newCalendar.get(Calendar.YEAR),
                selectedDate != null ? selectedDate.get(Calendar.MONTH) : newCalendar.get(Calendar.MONTH),
                selectedDate != null ? selectedDate.get(Calendar.DAY_OF_MONTH) : newCalendar.get(Calendar.DAY_OF_MONTH));

        picker.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_NEGATIVE) {
//                    if (getResources().getBoolean(R.bool.isTablet)) {
                    datePickerLarge.setEnabled(true);
//                    } else {
                    datePicker.setEnabled(true);
//                    }

                }
            }
        });

        picker.setCanceledOnTouchOutside(false);
        picker.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
        picker.show();
    }


    private void checkTermsAndConditions() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(this, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {

                    nccApi.getTermsAndConditionsAcepetence(BuildConfig.APIGEE_URL + "/profile/terms-and-conditions/terms-conditions/acceptance", "Bearer " + mPrefs.getString(NccConstants.APIGEE_TOKEN, "")).enqueue(new Callback<TermsAndConditionsAcceptenceModel>() {
                        @Override
                        public void onResponse(Call<TermsAndConditionsAcceptenceModel> call, Response<TermsAndConditionsAcceptenceModel> response) {
                            hideProgress();

                            if (response.code() == 403) {
                                tokenRefreshFailed();
                            }


                            if (response != null && response.code() == 404) {
                                mintlogEvent("Display Terms and Conditions");
                                showTermsAndConditions();
                            } else if (response.body() != null) {
                                mintlogEvent("Terms and Conditions Acceptance Success ");
                                checkPermissionsScreen();
                            } else if (response.errorBody() != null) {
                                try {
                                    JSONObject errorResponse = new JSONObject(response.errorBody().string());
                                    if (errorResponse != null) {
                                        if (errorResponse.get("message") != null && errorResponse.get("message").toString().length() > 1) {
                                            mintlogEvent("Terms and Conditions Acceptance Error - " + (String) errorResponse.get("message"));
                                            if (!isFinishing()) {
                                                Toast.makeText(HomeActivity.this, (String) errorResponse.get("message"), Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<TermsAndConditionsAcceptenceModel> call, Throwable t) {
                            hideProgress();
                            if (!isFinishing()) {
                                mintlogEvent("Terms and Conditions Acceptance Api Error - " + t.getLocalizedMessage());
                                Toast.makeText(HomeActivity.this, getString(R.string.auth_failed), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    tokenRefreshFailed();
                }
            });
        } else {
            mUserError.title = "";
            mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

    private void setArrowDropDownIcon(int arrowDropDown) {
        Drawable img = getResources().getDrawable(arrowDropDown);
        img.setBounds(0, 0, img.getIntrinsicWidth(), img.getIntrinsicHeight());
        if (getResources().getBoolean(R.bool.isTablet)) {
            mTextDutyLarge.setCompoundDrawables(null, null, img, null);
        } else {
            mTextDuty.setCompoundDrawables(null, null, img, null);
        }
    }

    private void checkPermissionsScreen() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                startActiveJobs();
            } else {
                mConstraintDuty.setVisibility(View.GONE);
                hideBottomBar();
                push(PermissionFragment.newInstance());
            }
        } else {
            startActiveJobs();
        }
    }

    private void showTermsAndConditions() {
        hideToolBar();
        push(TermsAndConditionsFragment.newInstance());
    }

    public void startActiveJobs() {
        isPermissionScreenClosed = true;
        mAlertBroadCastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // Here you can refresh your listview or other UI
                if (!isFinishing()) {
                    setBadgeCount(3, getUnReadAlertCount());
                }
            }
        };

        mChatBroadCastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // Here you can refresh your listview or other UI
                if (!isFinishing()) {
                    if (intent.hasExtra(NccConstants.CHAT_BADGE_COUNT)) {
                        updateBadge(intent.getIntExtra(NccConstants.CHAT_BADGE_COUNT, 0));
                    } else if (!TextUtils.isEmpty(NCCBadgeUpdateService.mRegisterThreadName) && intent.hasExtra(NccConstants.CHAT_MESSAGE_COUNT)) {
                        setMessageCountForThread();
                    }
                    reloadChat();
                }
            }
        };

        try {

            LocalBroadcastManager.getInstance(this).registerReceiver(mAlertBroadCastReceiver, new IntentFilter(NccConstants.ALERT_ACTION));
            LocalBroadcastManager.getInstance(this).registerReceiver(mChatBroadCastReceiver, new IntentFilter(NccConstants.CHAT_ACTION));

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        mConstraintDuty.setVisibility(View.VISIBLE);
        mUserError = new UserError();
        if (qBadgeView == null) {
            qBadgeView = new QBadgeView(this);
        }
        if (alertBadgeView == null) {
            alertBadgeView = new QBadgeView(this);
        }
        if (chatBadgeView == null) {
            chatBadgeView = new QBadgeView(this);
        }
        if (jobChatBadgeView == null) {
            jobChatBadgeView = new QBadgeView(this);
        }
        NCCApplication.getContext().getComponent().inject(this);
        setBadgeCount(3, getUnReadAlertCount());

        if (isFromChat && mIsJobCompleted) {
            mConstraintDuty.setVisibility(View.GONE);
            mBottomBar.setCurrentItem(1);
        } else {
            if (isLoggedInDriver()) {
                Timber.d("loading Driver Screen");
                if (isFromChat && !mIsJobCompleted) {
                    push(ActiveJobsFragment.newInstance(mJobId, true));
                } else {
                    push(ActiveJobsFragment.newInstance());
                }
            } else {
                Timber.d("loading Dispatcher Screen");
                if (isFromChat && !mIsJobCompleted) {
                    push(com.agero.ncc.dispatcher.fragments.ActiveJobsFragment.newInstance(mJobId, true));
                } else {
                    push(com.agero.ncc.dispatcher.fragments.ActiveJobsFragment.newInstance());
                }
            }
        }


    }

    private void registerUserForChat() {
        TokenManager.getInstance().validateToken(this, new TokenManager.TokenReponseListener() {
            @Override
            public void onRefreshSuccess() {
                FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

                final Map<String, Object> loginInfoMap = new HashMap<>();
                // Save the authentication ID for the current user
                // Set the current user

                String uid = firebaseUser.getUid();

                loginInfoMap.put(AuthKeys.CurrentUserID, uid);

                setLoginInfo(loginInfoMap);

                // Do a once() on the user to push its details to firebase.
                final UserWrapper userWrapper = UserWrapper.initWithAuthData(firebaseUser);

                userWrapper.getModel().setName(mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""));
                userWrapper.getModel().setEmail(mPrefs.getString(NccConstants.SIGNIN_EMAILADDRESS, ""));

                userWrapper.getModel().update();

                FirebaseEventHandler.shared().currentUserOn(userWrapper.getModel().getEntityID());


                if (NM.hook() != null) {
                    HashMap<String, Object> data = new HashMap<>();
                    data.put(BaseHookHandler.UserAuthFinished_User, userWrapper.getModel());
                    NM.hook().executeHook(BaseHookHandler.UserAuthFinished, data);
                }

                getChatMessages();
            }

            @Override
            public void onRefreshFailure() {
                hideProgress();
                tokenRefreshFailed();
            }
        });
    }

    public void setLoginInfo(Map<String, Object> values) {

        SharedPreferences.Editor keyValuesEditor = ChatSDK.shared().getPreferences().edit();

        for (String s : values.keySet()) {
            if (values.get(s) instanceof Integer)
                keyValuesEditor.putInt(s, (Integer) values.get(s));
            else if (values.get(s) instanceof String)
                keyValuesEditor.putString(s, (String) values.get(s));
            else if (values.get(s) instanceof Boolean)
                keyValuesEditor.putBoolean(s, (Boolean) values.get(s));
        }

        keyValuesEditor.apply();
    }

    public void getChatMessages() {
        if (NccConstants.IS_CHAT_ENABLED) {
            NM.events().source()
                    .filter(NetworkEvent.filterPrivateThreadsUpdated())
                    .subscribe(networkEvent -> {
                        startBadgeUpdateService();

                    });
        }
    }

    public int getUnReadAlertCount() {
        return nccDatabase.getAlertDao().getUnreadCount();
    }

    public void startBadgeUpdateService() {
//        Intent nccBadgeUpdateService = new Intent(HomeActivity.this, NCCBadgeUpdateService.class);
//        startService(nccBadgeUpdateService);
        NCCBadgeUpdateService.enqueueWork(HomeActivity.this, NCCBadgeUpdateService.class, 1, new Intent());
    }

    private void updateBadge(int count) {
        setBadgeCount(2, count);
        if (!TextUtils.isEmpty(NCCBadgeUpdateService.mRegisterThreadName)) {
            setMessageCountForThread();
        }
    }

    public void setCurrentThreadName(String threadName) {
        NCCBadgeUpdateService.mRegisterThreadName = threadName;
    }

    public void setMessageCountForThread() {
        int count = 0;
        for (Thread thread :
                NM.thread().getThreads(ThreadType.Private, false)) {
            if (thread.getUnreadMessagesCount() > 0 && NCCBadgeUpdateService.mRegisterThreadName.equalsIgnoreCase(thread.getDisplayName())) {
                count = thread.getUnreadMessagesCount();
                break;
            }
        }
        setJobChatBadge(count);
    }

    public void setJobChatBadge(int count) {
        if (count > 0) {
            jobChatBadgeView.setBadgeNumber(count);
        } else {
            jobChatBadgeView.hide(false);
        }
    }

    private void onDutyUploadComplete() {
        mEditor.putBoolean(NccConstants.SHOW_ON_OFF_DUTY, false).commit();
        mConstraintDuty.setVisibility(View.GONE);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        bottomBarSelected = savedInstanceState.getInt(NccConstants.BOTTOMBARSELECTED);
    }

    private int bottomBarSelected;

    @Override
    protected void onResume() {
        super.onResume();
        if (mBottomBar != null) {
            mBottomBar.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);
            mBottomBar.enableAnimation(false);
            mBottomBar.enableShiftingMode(false);
            mBottomBar.enableItemShiftingMode(false);
            mBottomBar.clearAnimation();
            mBottomBar.setAnimation(null);
            if (!isFragmentAvailable()) {
                mBottomBar.setCurrentItem(bottomBarSelected);
            }
        }
        startBadgeUpdateService();
        removeNewConversationFragment();
    }

    private boolean isFragmentAvailable() {
        boolean isVisible = false;
        List<Fragment> list = getSupportFragmentManager().getFragments();
        for (Fragment fragment :
                list) {
            if (fragment.isVisible()) {
                isVisible = true;
            }
        }
        return isVisible;
    }


    /* Remove the locationlistener updates when Activity is paused */
    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }

    private void initBadgeView() {
        BottomNavigationMenuView bottomNavigationMenuView =
                (BottomNavigationMenuView) mBottomBar.getChildAt(0);

        if (getResources().getBoolean(R.bool.isTablet)) {
            qBadgeView.bindTarget(bottomNavigationMenuView.findViewById(R.id.action_active)).setBadgeNumber(0).setBadgeGravity(Gravity.TOP | Gravity.END)
                    .setBadgeTextSize(12, true)
                    .setGravityOffset(60, 5, true);
        } else {
            qBadgeView.bindTarget(bottomNavigationMenuView.findViewById(R.id.action_active)).setBadgeNumber(0).setBadgeGravity(Gravity.TOP | Gravity.END)
                    .setBadgeTextSize(12, true)
                    .setGravityOffset(30, 5, true);
        }

        if (NccConstants.IS_CHAT_ENABLED) {
            if (getResources().getBoolean(R.bool.isTablet)) {
                chatBadgeView.bindTarget(bottomNavigationMenuView.findViewById(R.id.action_chat)).setBadgeNumber(0).setBadgeGravity(Gravity.TOP | Gravity.END)
                        .setBadgeTextSize(12, true)
                        .setGravityOffset(60, 5, true);
            } else {
                chatBadgeView.bindTarget(bottomNavigationMenuView.findViewById(R.id.action_chat)).setBadgeNumber(0).setBadgeGravity(Gravity.TOP | Gravity.END)
                        .setBadgeTextSize(12, true)
                        .setGravityOffset(30, 5, true);
            }
            if (getResources().getBoolean(R.bool.isTablet)) {
                jobChatBadgeView.bindTarget(mImageChatBadgeTargetLarge).setBadgeNumber(0).setBadgeGravity(Gravity.END | Gravity.TOP)
                        .setBadgeTextSize(12, true)
                        .setGravityOffset(0, 0, true);
            } else {
                jobChatBadgeView.bindTarget(mImageChatBadgeTarget).setBadgeNumber(0).setBadgeGravity(Gravity.END | Gravity.TOP)
                        .setBadgeTextSize(12, true)
                        .setGravityOffset(0, 0, true);
            }
        }

        if (getResources().getBoolean(R.bool.isTablet)) {
            alertBadgeView.bindTarget(bottomNavigationMenuView.findViewById(R.id.action_notifications)).setBadgeNumber(0).setBadgeGravity(Gravity.TOP | Gravity.END)
                    .setBadgeTextSize(12, true)
                    .setGravityOffset(60, 5, true);
        } else {
            alertBadgeView.bindTarget(bottomNavigationMenuView.findViewById(R.id.action_notifications)).setBadgeNumber(0).setBadgeGravity(Gravity.TOP | Gravity.END)
                    .setBadgeTextSize(12, true)
                    .setGravityOffset(30, 5, true);
        }


    }

    public void setBadgeCount(int index, int count) {

        if (index == 0) {
            if (count > 0) {
                if (getResources().getBoolean(R.bool.isTablet)) {
                    qBadgeView.setBadgeNumber(count);
                } else {
                    qBadgeView.setBadgeNumber(count);
                }
            } else {
                qBadgeView.hide(false);
            }
        } else if (index == 2) {
            if (count > 0) {
                if (getResources().getBoolean(R.bool.isTablet)) {
                    chatBadgeView.setBadgeNumber(count);
                } else {
                    chatBadgeView.setBadgeNumber(count);
                }
            } else {
                chatBadgeView.hide(false);
            }
        } else {
            if (count > 0) {
                if (getResources().getBoolean(R.bool.isTablet)) {
                    alertBadgeView.setBadgeNumber(count);
                } else {
                    alertBadgeView.setBadgeNumber(count);
                }
            } else {
                alertBadgeView.hide(false);
            }
        }
    }

    public void hideBadgeCount() {
        BottomNavigationMenuView bottomNavigationMenuView =
                (BottomNavigationMenuView) mBottomBar.getChildAt(0);
        View v = bottomNavigationMenuView.getChildAt(0);
        qBadgeView.bindTarget(v).hide(false);
    }

    public void equipmentSave(String save) {
        if (save.equalsIgnoreCase("save")) {
        } else {
        }

    }

    public void nextEnableDisable(boolean isEnable) {
        mTextNext.setEnabled(isEnable);
        mTextNext.setAlpha(isEnable ? 1.0f : 0.5f);
    }

    public void saveDisable() {
        mImageSave.setImageResource(R.drawable.ic_tick_grey);
        mImageSave.setEnabled(false);
    }

    public void saveEnable() {
        mImageSave.setImageResource(R.drawable.ic_vehicleconditionreport_tick);
        mImageSave.setEnabled(true);
    }

    public void hideSaveButton() {
        mImageSave.setVisibility(View.GONE);
    }

    public void showToolbar(String dutyToolbar, String screen) {

        toolbar.setVisibility(View.VISIBLE);
        mRelativeJobdetailLarge.setVisibility(View.GONE);
        enableDisableCall(true);
        enableDisableNotes(true);

        searchLayoutLarge.setVisibility(View.VISIBLE);
        searchLayout.setVisibility(View.VISIBLE);
        setDefaultToolbarHeight();

        if (dutyToolbar.startsWith(getString(R.string.title_job_id)) && screen.equalsIgnoreCase(getString(R.string.title_alertdetail))) {
            searchLayoutLarge.setVisibility(View.GONE);
            searchLayout.setVisibility(View.GONE);

            mRelativeDuty.setVisibility(View.VISIBLE);
            mTextNext.setVisibility(View.GONE);
            mRelativeJobdetail.setVisibility(View.GONE);
            mTextNext.setVisibility(View.GONE);
            mImageViewDutyClose.setVisibility(View.VISIBLE);
            mImageSave.setVisibility(View.VISIBLE);
            mRelativeSpinnerDuty.setVisibility(View.GONE);
            mConstarintEquipment.setVisibility(View.GONE);
            mDutySwitch.setVisibility(View.GONE);
            mToolbarText.setText(dutyToolbar);
            toolbar.setBackgroundColor(getResources().getColor(R.color.ncc_blue));
            mImageViewDutyClose.setImageResource(R.drawable.ic_backarrow);
            mImageViewDutyClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            mImageSave.setImageResource(R.drawable.ic_more_vert);
            mImageSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mAlertDetailToolbarLisener.onSettings();
                }
            });
        } else if (dutyToolbar.startsWith(getString(R.string.title_job_id))
                && (screen.equalsIgnoreCase(getString(R.string.title_jobdetail)) || screen.equalsIgnoreCase(getString(R.string.title_history)) ||
                screen.equalsIgnoreCase(getString(R.string.title_firestore_history)))) {

            mRelativeDuty.setVisibility(View.GONE);
            mRelativeJobdetail.setVisibility(View.VISIBLE);
            mTextNext.setVisibility(View.GONE);
            mImageSearchIconLarge.setVisibility(View.GONE);
//            mConstraintDuty.setVisibility(View.GONE);
//            mConstarintEquipment.setVisibility(View.GONE);
            mRelativeSpinnerDuty.setVisibility(View.GONE);
            mToolbarJobdetailText.setText(dutyToolbar);
            mImageNotes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mJobDetailToolbarLisener.onNotes();
                }
            });
            mImageNotesLarge.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mJobDetailToolbarLisener.onNotes();
                }
            });
            mImageCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mJobDetailToolbarLisener.onCall();
                }
            });
            mImageCallLarge.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mJobDetailToolbarLisener.onCall();
                }
            });
            if (getResources().getBoolean(R.bool.isTablet)) {
                mRelativeJobdetailLarge.setVisibility(View.VISIBLE);
                mRelativeJobdetail.setVisibility(View.GONE);
                mTextDutyLarge.setVisibility(View.VISIBLE);
                mTextHistoryLarge.setVisibility(View.GONE);
                //searchLayoutLarge.setVisibility(View.GONE);
                if (screen.equalsIgnoreCase(getString(R.string.title_firestore_history))) {
                    searchLayoutLarge.setVisibility(View.VISIBLE);
                    searchLayout.setVisibility(View.GONE);
                    setSearchToolbarHeight();
                    mTextDutyLarge.setVisibility(View.GONE);
                    //mTextHistoryLarge.setVisibility(View.VISIBLE);
                    //mTextHistoryLarge.setText(screen);
                } else if (screen.equalsIgnoreCase(getString(R.string.title_history))) {
                    searchLayoutLarge.setVisibility(View.GONE);
                    searchLayout.setVisibility(View.GONE);
                    setDefaultToolbarHeight();
                    mTextDutyLarge.setVisibility(View.GONE);
                    mTextHistoryLarge.setVisibility(View.VISIBLE);
                    mTextHistoryLarge.setText(screen);
                    mImageSearchIconLarge.setVisibility(View.VISIBLE);
                    mImageSearchIconLarge.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mToolbarSaveListener != null) {
                                mToolbarSaveListener.onSave();
                            }
                        }
                    });
                } else {
                    searchLayoutLarge.setVisibility(View.GONE);
                    searchLayout.setVisibility(View.GONE);
                    setDefaultToolbarHeight();

                    if (!(isLoggedInDriver() || isUserDispatcherAndDriver())) {
                        mTextDutyLarge.setVisibility(View.GONE);
                        mTextHistoryLarge.setVisibility(View.VISIBLE);
                        mTextHistoryLarge.setText(getString(R.string.my_jobs_title));
                    } else {
                        setArrowDropDownIcon(R.drawable.ic_arrow_drop_down_white_24px);
                    }
                }
            } else {
                mRelativeJobdetailLarge.setVisibility(View.GONE);
                mRelativeJobdetail.setVisibility(View.VISIBLE);
                mImageBackArrow.setVisibility(View.VISIBLE);
                mImageBackArrow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });

                if (screen.equalsIgnoreCase(getString(R.string.title_firestore_history))) {
                    searchLayoutLarge.setVisibility(View.GONE);
                    searchLayout.setVisibility(View.VISIBLE);
                    setSearchToolbarHeight();
                } else {
                    searchLayoutLarge.setVisibility(View.GONE);
                    searchLayout.setVisibility(View.GONE);
                    setDefaultToolbarHeight();
                }
            }

            /*if (isLoggedInDriver()) {
                jobOptionsVisible(false);
            } else {*/
            jobOptionsVisible(true);
            mImageDispatcherOptions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mJobDetailToolbarLisener.onOptions();
                }
            });
            mImageDispatcherOptionsLarge.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mJobDetailToolbarLisener.onOptions();
                }
            });
            //}

        } else if (screen.equalsIgnoreCase(getString(R.string.vehicle_report_tag_title))) {

            searchLayoutLarge.setVisibility(View.GONE);
            searchLayout.setVisibility(View.GONE);

            mRelativeSpinnerDuty.setVisibility(View.GONE);
            mTextNext.setVisibility(View.VISIBLE);
            mConstraintDuty.setVisibility(View.GONE);
            mRelativeDuty.setVisibility(View.VISIBLE);
            mRelativeJobdetail.setVisibility(View.GONE);
            mImageViewDutyClose.setVisibility(View.VISIBLE);
            mImageSave.setVisibility(View.GONE);
            mImageViewDutyClose.setImageResource(R.drawable.ic_backarrow);
            mConstarintEquipment.setVisibility(View.GONE);
            mDutySwitch.setVisibility(View.GONE);
            mToolbarText.setText(dutyToolbar);
            mImageViewDutyClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mToolbarSaveCancelListener != null) {
                        removeAllVehicleFragment();
                        mToolbarSaveCancelListener.onCancel();
                    }
                }
            });
            nextEnableDisable(false);

            mTextNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mToolbarSaveCancelListener != null) {
                        mToolbarSaveCancelListener.onSave();
                    }
                }
            });
        }
    }

    public void jobCallVisible(boolean isVisible) {
        if (isVisible) {
            mImageCall.setVisibility(View.VISIBLE);
            mImageCallLarge.setVisibility(View.VISIBLE);
        } else {
            mImageCall.setVisibility(View.GONE);
            mImageCallLarge.setVisibility(View.GONE);
        }
    }

    public void jobOptionsVisible(boolean isVisible) {
        if (isVisible) {
            mImageDispatcherOptions.setVisibility(View.VISIBLE);
            mImageDispatcherOptionsLarge.setVisibility(View.VISIBLE);
        } else {
            mImageDispatcherOptions.setVisibility(View.GONE);
            mImageDispatcherOptionsLarge.setVisibility(View.GONE);
        }
    }

    public void enableDisableNotes(boolean isEnable) {
        if (isEnable) {
            mImageNotes.setAlpha(1f);
            mImageNotes.setEnabled(true);
            mImageNotesLarge.setAlpha(1f);
            mImageNotesLarge.setEnabled(true);

            if (NccConstants.IS_CHAT_ENABLED) {
                mImageNotes.setImageResource(R.drawable.ic_chat_white);
                mImageNotesLarge.setImageResource(R.drawable.ic_chat_white);
            } else {
                mImageNotes.setImageResource(R.drawable.ic_notes_white);
                mImageNotesLarge.setImageResource(R.drawable.ic_notes_white);
            }

        } else {
            mImageNotes.setVisibility(View.VISIBLE);
            mImageNotes.setAlpha(0.5f);
            mImageNotes.setEnabled(false);

            if (NccConstants.IS_CHAT_ENABLED) {
                mImageNotes.setImageResource(R.drawable.ic_chat_light_white);
                mImageNotesLarge.setImageResource(R.drawable.ic_chat_light_white);
            } else {
                mImageNotes.setImageResource(R.drawable.ic_notes_light_white);
                mImageNotesLarge.setImageResource(R.drawable.ic_notes_light_white);
            }
            mImageNotesLarge.setVisibility(View.VISIBLE);
            mImageNotesLarge.setAlpha(0.5f);
            mImageNotesLarge.setEnabled(false);

        }
    }

    public void showHideNotes(boolean isShow) {
        if (isShow) {
            mImageNotes.setVisibility(View.VISIBLE);
            mImageNotesLarge.setVisibility(View.VISIBLE);
        } else {
            mImageNotes.setVisibility(View.GONE);
            mImageNotesLarge.setVisibility(View.GONE);
        }
    }

    public void enableDisableCall(boolean isEnable) {
        if (isEnable) {
            mImageCall.setAlpha(1f);
            mImageCall.setImageResource(R.drawable.ic_toolbar_call);
            mImageCall.setEnabled(true);
            mImageCallLarge.setAlpha(1f);
            mImageCallLarge.setImageResource(R.drawable.ic_toolbar_call);
            mImageCallLarge.setEnabled(true);
        } else {
            mImageCall.setVisibility(View.VISIBLE);
            mImageCall.setAlpha(0.5f);
            mImageCall.setEnabled(false);
            mImageCall.setImageResource(R.drawable.ic_call_light_white);
            mImageCallLarge.setVisibility(View.VISIBLE);
            mImageCallLarge.setAlpha(0.5f);
            mImageCallLarge.setEnabled(false);
            mImageCallLarge.setImageResource(R.drawable.ic_call_light_white);
        }
    }

    public void setBGClickableFalse() {
        if (mConstraintDuty.getVisibility() == View.GONE) {
            mConstraintDuty.setClickable(false);
            mConstraintDuty.setEnabled(false);
            mConstraintDuty.setFocusable(false);
        } else {
            mConstraintDuty.setClickable(true);
            mConstraintDuty.setEnabled(true);
            mConstraintDuty.setFocusable(true);
        }
    }

    public void showToolbar(String dutyToolbar) {
        toolbar.setVisibility(View.VISIBLE);
        mRelativeJobdetailLarge.setVisibility(View.GONE);
        enableDisableCall(true);
        enableDisableNotes(true);

        searchLayoutLarge.setVisibility(View.VISIBLE);
        searchLayout.setVisibility(View.VISIBLE);
        setDefaultToolbarHeight();

        if (dutyToolbar.equalsIgnoreCase(getString(R.string.toolbar_text_duty))) {
            searchLayoutLarge.setVisibility(View.GONE);
            searchLayout.setVisibility(View.GONE);

            mRelativeDuty.setVisibility(View.GONE);
            mConstraintDuty.setVisibility(View.GONE);
            if (isUserDispatcherAndDriver() || isLoggedInDriver()) {
                mRelativeSpinnerDuty.setVisibility(View.VISIBLE);
                mTextDutyMyJobs.setVisibility(View.GONE);
                mTextDuty.setVisibility(View.VISIBLE);
            } else {
                mRelativeSpinnerDuty.setVisibility(View.VISIBLE);
                mTextDutyMyJobs.setVisibility(View.VISIBLE);
                mTextDuty.setVisibility(View.GONE);
                mTextDutyMyJobs.setText(getString(R.string.my_jobs_title));
            }
            mRelativeJobdetail.setVisibility(View.GONE);
            mImageViewDutyClose.setVisibility(View.GONE);
            mDutySwitch.setVisibility(View.VISIBLE);
            mImageSave.setVisibility(View.GONE);
            toolbar.setBackgroundColor(getResources().getColor(R.color.ncc_blue));
            mConstarintEquipment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            mDutySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (mDutySwitch != null && mDutySwitch.isPressed()) {
                        if (isChecked) {
                            eventAction = NccConstants.FirebaseEventAction.GO_ON_DUTY;
                            if (isEquipmentAvailable()) {
                                setDuty(getString(R.string.title_onduty));
                                if (Utils.isNetworkAvailable()) {
                                    showProgress();
                                    TokenManager.getInstance().validateToken(HomeActivity.this, new TokenManager.TokenReponseListener() {
                                        @Override
                                        public void onRefreshSuccess() {
                                            hideProgress();
                                            uploadDuty(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""), true, null, false);
                                        }

                                        @Override
                                        public void onRefreshFailure() {
                                            hideProgress();
                                            tokenRefreshFailed();
                                        }
                                    });
                                } else {
                                    hideProgress();
                                    mUserError.title = "";
                                    mUserError.message = getString(R.string.network_error_message);
                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                }

                            } else {
                                push(EquipmentSelectionFragment.newInstance(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""), true), getString(R.string.title_select_equipment));
                            }


                        } else {
                            eventAction = NccConstants.FirebaseEventAction.GO_OFF_DUTY;
                            if ((isLoggedInDriver() || isUserDispatcherAndDriver()) && mPrefs.getInt("jobsize", 0) > 0) {
                                mDutySwitch.setChecked(true);
                                showGoOffDutyDialog(false);
                                setArrowDropDownIcon(R.drawable.ic_arrow_drop_down_white_24px);
                            } else {
                                setDuty(getString(R.string.title_offduty));
                                if (Utils.isNetworkAvailable()) {
                                    showProgress();
                                    TokenManager.getInstance().validateToken(HomeActivity.this, new TokenManager.TokenReponseListener() {
                                        @Override
                                        public void onRefreshSuccess() {
                                            hideProgress();
                                            uploadDuty(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""), false, mPrefs.getString(NccConstants.EQUIPMENT_ID, "-1"), false);
                                        }

                                        @Override
                                        public void onRefreshFailure() {
                                            hideProgress();
                                            tokenRefreshFailed();
                                        }
                                    });
                                } else {
                                    hideProgress();
                                    mUserError.title = "";
                                    mUserError.message = getString(R.string.network_error_message);
                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                }

                            }
                        }
                    }
                    firebaseLogEvent(eventCategory, eventAction);
                }
            });

            if (mPrefs.getBoolean(NccConstants.SHOW_ON_OFF_DUTY, false)) {
                mConstraintDuty.setVisibility(View.VISIBLE);
            } else {
                mConstraintDuty.setVisibility(View.GONE);
            }
            setBGClickableFalse();

            if (getResources().getBoolean(R.bool.isTablet)) {
                setDuty(mTextDutyLarge.getText().toString().trim());
            } else {
                setDuty(mTextDuty.getText().toString().trim());
            }
        } else if (dutyToolbar.equalsIgnoreCase(getString(R.string.title_new_equipment))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_add_photos))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_vehicle_condition_report))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_vehicle_condition_report_history))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_create_job))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_job_modify_status))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_job_status))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_updated_password))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_add_company))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_facility))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_service_unsuccessful))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.edit_service_label))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_edit_profile))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_chat))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.new_conversation))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_disablement_sign))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_dropoff_sign))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_scanvin))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_edit_tow))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_select_equipment))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_edit_disablement))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_assign_driver))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.alert_label_update_eta))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_history))) {

            searchLayoutLarge.setVisibility(View.GONE);
            searchLayout.setVisibility(View.GONE);

            mRelativeSpinnerDuty.setVisibility(View.GONE);
            mConstraintDuty.setVisibility(View.GONE);
            mRelativeDuty.setVisibility(View.VISIBLE);
            mTextNext.setVisibility(View.GONE);
            mRelativeJobdetail.setVisibility(View.GONE);
            if (dutyToolbar.equalsIgnoreCase(getString(R.string.title_edit_profile)) && getResources().getBoolean(R.bool.isTablet)) {
                mImageViewDutyClose.setVisibility(View.GONE);
            } else {
                mImageViewDutyClose.setVisibility(View.VISIBLE);
            }
            if (dutyToolbar.equalsIgnoreCase(getString(R.string.title_job_status)) || dutyToolbar.equalsIgnoreCase(getString(R.string.title_assign_driver))) {
                mImageSave.setVisibility(View.GONE);
            } else {
                mImageSave.setVisibility(View.VISIBLE);
            }
            mConstarintEquipment.setVisibility(View.GONE);
            mDutySwitch.setVisibility(View.GONE);
            mToolbarText.setText(dutyToolbar);
            mImageViewDutyClose.setImageResource(R.drawable.ic_close_white_24dp);
            mImageViewDutyClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeAllVehicleFragment();
                    onBackPressed();

                }
            });

            saveEnable();
            if (dutyToolbar.equalsIgnoreCase(getString(R.string.title_facility))) {
                if (getResources().getBoolean(R.bool.isTablet)) {
                    mImageViewDutyClose.setVisibility(View.GONE);
                } else {
                    mImageViewDutyClose.setVisibility(View.VISIBLE);
                    mImageSave.setImageResource(R.drawable.ic_more_vert);
                    mImageViewDutyClose.setImageResource(R.drawable.ic_backarrow);
                }
            } else {
                mImageSave.setImageResource(R.drawable.ic_vehicleconditionreport_tick);
            }
            mImageSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mToolbarSaveListener != null) {
                        mToolbarSaveListener.onSave();
                    }
                }
            });

            if (dutyToolbar.equalsIgnoreCase(getString(R.string.title_chat))) {
                mImageViewDutyClose.setVisibility(View.GONE);
                mImageSave.setImageResource(R.drawable.ic_chat_new);
            }
            if (dutyToolbar.equalsIgnoreCase(getString(R.string.title_history))) {
                mImageViewDutyClose.setVisibility(View.GONE);
                mImageSave.setImageResource(R.drawable.ic_search_job_icon_white);
            }

        } else if (dutyToolbar.equalsIgnoreCase(getString(R.string.title_company))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_setting))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_help))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_legal))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_notes))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_additional_details))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_job_detail_map))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_add_vehicle))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_add_tow))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_add_customer))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_add_job))) {

            searchLayoutLarge.setVisibility(View.GONE);
            searchLayout.setVisibility(View.GONE);

            mRelativeSpinnerDuty.setVisibility(View.GONE);
            mConstraintDuty.setVisibility(View.GONE);
            mRelativeDuty.setVisibility(View.VISIBLE);
            mTextNext.setVisibility(View.GONE);
            mRelativeJobdetail.setVisibility(View.GONE);
            if ((dutyToolbar.equalsIgnoreCase(getString(R.string.title_help)) ||
                    dutyToolbar.equalsIgnoreCase(getString(R.string.title_legal)))
                    && getResources().getBoolean(R.bool.isTablet)) {
                mImageViewDutyClose.setVisibility(View.GONE);
            } else {
                mImageViewDutyClose.setVisibility(View.VISIBLE);
            }

            mConstarintEquipment.setVisibility(View.GONE);
            mDutySwitch.setVisibility(View.GONE);
            mToolbarText.setText(dutyToolbar);
            toolbar.setBackgroundColor(getResources().getColor(R.color.ncc_blue));
            mImageViewDutyClose.setImageResource(R.drawable.ic_backarrow);
            mImageViewDutyClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dutyToolbar.equalsIgnoreCase(getString(R.string.title_edit_profile))) {
                        if (mToolbarSaveCancelListener != null) {
                            mToolbarSaveCancelListener.onCancel();
                        }
                    } else {
                        onBackPressed();
                    }
                }
            });
            mImageSave.setVisibility(View.GONE);
        } else if (dutyToolbar.equalsIgnoreCase(getString(R.string.title_terms_conditions))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_alerts))
                || dutyToolbar.equalsIgnoreCase(getString(R.string.title_account))) {

            searchLayoutLarge.setVisibility(View.GONE);
            searchLayout.setVisibility(View.GONE);

            mRelativeSpinnerDuty.setVisibility(View.GONE);
            mConstraintDuty.setVisibility(View.GONE);
            mRelativeDuty.setVisibility(View.VISIBLE);
            mTextNext.setVisibility(View.GONE);
            mRelativeJobdetail.setVisibility(View.GONE);
            mImageViewDutyClose.setVisibility(View.GONE);
            mConstarintEquipment.setVisibility(View.GONE);
            mDutySwitch.setVisibility(View.GONE);
            mToolbarText.setText(dutyToolbar);
            toolbar.setBackgroundColor(getResources().getColor(R.color.ncc_blue));
            mImageViewDutyClose.setImageResource(R.drawable.ic_backarrow);
            mImageSave.setVisibility(View.GONE);

        } else if (dutyToolbar.equalsIgnoreCase(getString(R.string.title_firestore_history))) {
            mRelativeSpinnerDuty.setVisibility(View.GONE);
            mConstraintDuty.setVisibility(View.GONE);
            mRelativeDuty.setVisibility(View.GONE);
            mTextNext.setVisibility(View.GONE);
            mRelativeJobdetail.setVisibility(View.GONE);
            mImageViewDutyClose.setVisibility(View.GONE);
            mConstarintEquipment.setVisibility(View.GONE);
            mDutySwitch.setVisibility(View.GONE);
            mToolbarText.setText(dutyToolbar);
            toolbar.setBackgroundColor(getResources().getColor(R.color.ncc_blue));
            mImageViewDutyClose.setImageResource(R.drawable.ic_backarrow);
            mImageSave.setVisibility(View.GONE);
            mTextHistoryLarge.setVisibility(View.GONE);

            if (getResources().getBoolean(R.bool.isTablet)) {
                searchLayoutLarge.setVisibility(View.GONE);
                searchLayout.setVisibility(View.VISIBLE);
            } else {
                searchLayoutLarge.setVisibility(View.GONE);
                searchLayout.setVisibility(View.VISIBLE);
            }

            setSearchToolbarHeight();
        }
    }

    /*
    adjust size of the toolbar enough to support searchview
     */
    private void setSearchToolbarHeight() {
        int height = 0;
        if (getResources().getBoolean(R.bool.isTablet)) {
            height = searchLayoutLarge.getLayoutParams().height;
        } else {
            height = searchLayout.getLayoutParams().height;
        }
        toolbar.setMinimumHeight(height);

    }

    /*
    reset toolbar height to default
     */
    private void setDefaultToolbarHeight() {

        final TypedArray styledAttributes = getTheme().obtainStyledAttributes(
                new int[]{android.R.attr.actionBarSize});
        int mActionBarSize = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();

        toolbar.setMinimumHeight(mActionBarSize);
    }

    @Override
    public void onBackPressed() {
        if (mToolbarSaveCancelListener != null) {
            mToolbarSaveCancelListener.onCancel();
        } else {
            try {
                removeAllVehicleFragment();
                super.onBackPressed();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setDuty(String duty) {

        if (isUserDispatcherAndDriver() || isLoggedInDriver()) {
            if (getResources().getBoolean(R.bool.isTablet)) {
                mTextDutyLarge.setText(duty);
                mTextDuty.setText(duty);
            } else {
                mTextDuty.setText(duty);
            }
            if (duty.equalsIgnoreCase(getString(R.string.title_offduty))) {
                mDutySwitch.setChecked(false);
                setEquipmentVisibility(false);
                if (onDutyToolbarListener != null) {
                    onDutyToolbarListener.offDutyCall();
                }

            } else {
                mDutySwitch.setChecked(true);
                setEquipmentVisibility(true);
                if (onDutyToolbarListener != null) {
                    onDutyToolbarListener.onDutyCall();
                }
            }
        } else {
            if (getResources().getBoolean(R.bool.isTablet)) {
                mTextDutyLarge.setText(getString(R.string.my_jobs_title));
            } else {
                mTextDutyMyJobs.setVisibility(View.VISIBLE);
                mTextDuty.setVisibility(View.GONE);
                mTextDutyMyJobs.setText(getString(R.string.my_jobs_title));
            }

        }


    }

    public void uploadDuty(String userId, boolean duty, String equipmentId, boolean isLogout, DutyUpdateListener dutyUpdateListener) {

//        SparkLocationUpdateService.onDutyFlag = duty;

        if (Utils.isNetworkAvailable()) {
            showProgress();
            serviceUnFinished++;
            FirebaseDatabase database = FirebaseDatabase.getInstance();

            DatabaseReference reference = database.getReference("Users/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "")
                    + "/" + userId + "/" + "onDuty");
            //reference.keepSynced(true);
/*
            RxFirebaseDatabase.setValue(reference, duty).subscribe(() -> {
                serviceUnFinished--;
                if (!duty && equipmentId != null) {
                    if (!"-1".equalsIgnoreCase(equipmentId)) {
                        setEquipmentAvailable(equipmentId, isLogout);
                        clearEquipment(userId, isLogout);
                    } else if (isLogout) {
                        clearSigninData();
                    }

                } else {
                    if (serviceUnFinished <= 0) {
                        hideProgress();
                        if (isLogout) {
                            clearSigninData();
                        }
                    }
                }

                if (userId.equalsIgnoreCase(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""))) {
                    SparkLocationUpdateService.onDutyFlag = duty;
                    if (duty) {
                        startSparkLocationService();
                    } else {
                        stopSparkLocationService();
                    }
                }

                if (dutyUpdateListener != null) {
                    dutyUpdateListener.onSuccess();
                }

            }, throwable -> {
                hideProgress();
                if (!isFinishing()) {
                    mintlogEvent("Change Duty Status Firebase Database Error - " + throwable.getLocalizedMessage());
                    if (dutyUpdateListener != null) {
                        dutyUpdateListener.onFailure();
                    }
                }
            });*/

            HashMap<String, Object> dutyObj = new HashMap<String, Object>();
            dutyObj.put("driverId", userId);
            dutyObj.put("onDuty", duty);
            dutyObj.put("userId", mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
            dutyObj.put("authToken", mPrefs.getString(NccConstants.APIGEE_TOKEN, ""));
            dutyObj.put("subSource", NccConstants.SUB_SOURCE);

            HashMap<String, String> extraDatas = new HashMap<>();
            extraDatas.put("json", new Gson().toJson(dutyObj));
            mintlogEventExtraData("driver_status_change", extraDatas);

            FirebaseFunctions.getInstance()
                    .getHttpsCallable(NccConstants.API_DRIVER_STATUS_UPDATE)
                    .call(dutyObj)
                    .addOnFailureListener(new OnFailureListener() {

                        @Override
                        public void onFailure(@NonNull Exception e) {
                            if (!isFinishing()) {
                                hideProgress();
                                Timber.d("failed");
                                e.printStackTrace();
                                UserError mUserError = new UserError();
                                if (e instanceof FirebaseFunctionsException) {
                                    FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                                    FirebaseFunctionsException.Code code = ffe.getCode();
                                    Object details = ffe.getDetails();
                                    mintlogEvent("driver_status_change: " + " Firebase Functions Error - Error Message -" + ffe.getMessage());

                                    mUserError.message = ffe.getMessage();
                                    try {
                                        JSONObject errorObj = new JSONObject(ffe.getMessage());
                                        mUserError.title = errorObj.optString("title");
                                        mUserError.message = errorObj.optString("body");
                                        showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);

                                    } catch (Exception parseException) {
                                        parseException.printStackTrace();
                                        showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                    }
                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);


                                } else if (e.getMessage() != null) {
                                    if (getString(R.string.firebase_logout_error_msg).equalsIgnoreCase(e.getMessage())) {
                                        tokenRefreshFailed();
                                    } else {
                                        mUserError.message = e.getMessage();
                                        showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                        mintlogEvent("driver_status_change: " + " Firebase Functions Error - Error Message -" + e.getMessage());
                                    }
                                } else {
                                    mintlogEvent("driver_status_change Firebase Functions Error - Error Message -  failed");
                                    mUserError.message = "failed";
                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                }
                                if (dutyUpdateListener != null) {
                                    dutyUpdateListener.onFailure();
                                }
                            }
                        }
                    })
                    .addOnSuccessListener(new OnSuccessListener<HttpsCallableResult>() {
                        @Override
                        public void onSuccess(HttpsCallableResult httpsCallableResult) {
                            if (!isFinishing()) {
                                hideProgress();
                                Timber.d("Successful");
                                serviceUnFinished--;
                                if (!duty && equipmentId != null) {
                                    if (!"-1".equalsIgnoreCase(equipmentId)) {
                                        setEquipmentAvailable(equipmentId, isLogout);
                                        clearEquipment(userId, isLogout);
                                    } else if (isLogout) {
                                        clearSigninData();
                                    }

                                } else {
                                    if (serviceUnFinished <= 0) {
                                        hideProgress();
                                        if (isLogout) {
                                            clearSigninData();
                                        }
                                    }
                                }

                                if (userId.equalsIgnoreCase(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""))) {
                                    SparkLocationUpdateService.onDutyFlag = duty;
                                    if (duty) {
                                        startSparkLocationService();
                                    } else {
                                        stopSparkLocationService();
                                    }
                                }

                                if (dutyUpdateListener != null) {
                                    dutyUpdateListener.onSuccess();
                                }
                            }
                        }
                    });

        } else {
            mUserError.title = "";
            mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

    public void uploadDuty(String userId, boolean duty, String equipmentId, boolean isLogout) {
        uploadDuty(userId, duty, equipmentId, isLogout, null);
    }

    public void updateProfileStatus(String userId, String status) {
        if (Utils.isNetworkAvailable()) {
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "")
                    + "/" + userId + "/" + "status");
            RxFirebaseDatabase.setValue(reference, status).subscribe(() -> {
                Timber.d("Success");
            }, throwable -> {
                Timber.d("Failure");
            });
        } else {
            mUserError.title = "";
            mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

    public void setEquipmentAvailable(String equipmentId, boolean isLogout) {
        serviceUnFinished++;

        HashMap<String, Object> value = new HashMap<>();
        value.put("isAvailable", true);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference("Equipment/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""));
        //databaseReference.keepSynced(true);
        RxFirebaseDatabase.updateChildren(databaseReference.child(equipmentId), value).subscribe(() -> {
            serviceUnFinished--;
            if (serviceUnFinished <= 0) {
                hideProgress();
                if (isLogout) {
                    clearSigninData();
                }
            }
        }, throwable -> {
            hideProgress();
        });
    }

    public void clearEquipment(String userId, boolean isLogout) {

        serviceUnFinished++;

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference reference = database.getReference("Users/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "")
                + "/" + userId + "/Equipment");
        //reference.keepSynced(true);

        RxFirebaseDatabase.setValue(reference, null).subscribe(() -> {
            serviceUnFinished--;
            if (serviceUnFinished <= 0) {
                hideProgress();
                if (isLogout) {
                    clearSigninData();
                }
            }
        }, throwable -> {
            hideProgress();
        });
    }

    private boolean isEquipmentAvailable() {

        String equipmentJson = mPrefs.getString(NccConstants.EQUIPMENT_PREF_KEY, "");
        String equipmentId = mPrefs.getString(NccConstants.EQUIPMENT_ID, "-1");

        if (!equipmentId.equalsIgnoreCase("-1") && !TextUtils.isEmpty(equipmentJson)) {
            Equipment mEquipmentResults = new Gson().fromJson(equipmentJson, Equipment.class);
            mTextActiveDuty.setText(mEquipmentResults.getEquipmentType());
            if (mEquipmentResults.getName().isEmpty()) {

            } else {

            }
            return true;
        } else {
            return false;
        }

//        return false;
    }

    public void clearSigninData() {
        SparkLocationUpdateService.clearList();
        removePushToken();
        stopSparkLocationService();
        mintlogEvent("Sign out");
        if (NccConstants.IS_CHAT_ENABLED) {
            modifyPushToken();

        } else {
            TokenManager.getInstance().signOut();
            clearLoginPref();
        }

    }

    private void modifyPushToken() {
        if (!TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""))
                && !TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""))) {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference reference = database.getReference("Users/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "")
                    + "/" + mPrefs.getString(NccConstants.SIGNIN_USER_ID, "") + "/meta/pushToken");
            reference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    reference.removeEventListener(this);
                    String pushToken = (String) dataSnapshot.getValue();
                    logoutChat(pushToken);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else {
            logoutChat("");
        }
    }

    private void logoutChat(String pushToken) {
        if (NetworkManager.shared().a != null) {
            if (NM.core() != null && NM.currentUser() != null) {
                if (TextUtils.isEmpty(pushToken) || FirebaseInstanceId.getInstance().getToken().equalsIgnoreCase(pushToken)) {
                    NM.currentUser().setMetaString("pushToken", "");
                } else {
                    NM.currentUser().setMetaString("pushToken", pushToken);
                }
                StorageManager.shared().clearAllThreads();
            }
            if (NM.auth() != null) {
                NM.auth().logout()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(() -> {
                            Timber.d("logout");
                            TokenManager.getInstance().signOut();
                            clearLoginPref();

                        }, throwable -> {
                            ChatSDK.logError(throwable);
                            Timber.d("logout issue");
                            TokenManager.getInstance().signOut();
                            clearLoginPref();
                        });
            } else {
                TokenManager.getInstance().signOut();
                clearLoginPref();
            }
        } else {
            TokenManager.getInstance().signOut();
            clearLoginPref();
        }
    }

    private void clearLoginPref() {
        mIsInitialised = false;
        mIsRegistered = false;
        ChatHistoryFragment.mIsTotalUserInserted = false;
        mEditor.remove(NccConstants.EQUIPMENT_PREF_KEY).commit();
        mEditor.remove(NccConstants.EQUIPMENT_POSITION).commit();
        //mEditor.remove(NccConstants.SIGNIN_PASSWORD).commit();
        mEditor.remove(NccConstants.SIGNIN_USER_ROLE).commit();
        mEditor.remove(NccConstants.IS_OKGOTIT).commit();
        mEditor.remove(NccConstants.IS_ONDUTYCHECKED).commit();
        mEditor.remove(NccConstants.SHOW_ON_OFF_DUTY).commit();
        mEditor.remove(NccConstants.EQUIPMENT_ID).commit();
        mEditor.remove(NccConstants.DISPATCHER_NUMBER).commit();


        Intent intent = new Intent(HomeActivity.this, WelcomeActivity.class);
        startActivity(intent);
        finish();
    }

    public void setEquipmentVisibility(boolean isOnDuty) {
        if (isOnDuty) {
            mConstarintEquipment.setVisibility(View.VISIBLE);
            String equipmentJson = mPrefs.getString(NccConstants.EQUIPMENT_PREF_KEY, "");
            String equipmentId = mPrefs.getString(NccConstants.EQUIPMENT_ID, "-1");

            if (!equipmentId.equalsIgnoreCase("-1") && !TextUtils.isEmpty(equipmentJson)) {
                Equipment mEquipmentResults = new Gson().fromJson(equipmentJson, Equipment.class);
                mTextActiveDuty.setText(mEquipmentResults.getEquipmentType());
                if (mEquipmentResults.getName().isEmpty()) {
                    mTextActiveTruck.setText(mEquipmentResults.getPlateState()
                            + "-" + mEquipmentResults.getPlate());
                } else {
                    mTextActiveTruck.setText(mEquipmentResults.getPlateState()
                            + "-" + mEquipmentResults.getPlate() + " "
                            + getString(R.string.dot) + " " + mEquipmentResults.getName());
                }

                mTextDone.setEnabled(true);
                mTextDone.setAlpha(1f);
            } else {
                mTextActiveDuty.setText(getString(R.string.toolbar_noequipment));
                mTextActiveTruck.setText(getString(R.string.toolbar_select_equipment));
                mTextDone.setEnabled(false);
                mTextDone.setAlpha(0.5f);
            }
        } else {
            mTextDone.setEnabled(true);
            mTextDone.setAlpha(1f);
            mConstarintEquipment.setVisibility(View.GONE);
        }
    }

    public void hideToolBar() {
        toolbar.setVisibility(View.GONE);
        mConstarintEquipment.setVisibility(View.GONE);
    }

    public void hideDuty() {
        mConstraintDuty.setVisibility(View.GONE);
    }

    public View getCompanyOptionView() {
        return mImageSave;
    }

    public void showBottomBar() {
        mBottomBar.setVisibility(View.VISIBLE);
    }

    public void hideBottomBar() {
        mBottomBar.setVisibility(View.GONE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(NccConstants.KEY_FROM_SCREEN, isPermissionScreenClosed);
        bottomBarSelected = mBottomBar.getCurrentItem();
        outState.putInt(NccConstants.BOTTOMBARSELECTED, bottomBarSelected);
    }

    public void setOnDutyToolbarListener(OnDutyToolbarListener onDutyToolbarListener) {
        this.onDutyToolbarListener = onDutyToolbarListener;
    }

    public void setOnToolbarSaveListener(ToolbarSaveListener onToolbarSaveListener) {
        this.mToolbarSaveListener = onToolbarSaveListener;
    }

    public void setOnToolbarSaveCancelListener(ToolbarSaveCancelListener onToolbarSaveCancelListener) {
        this.mToolbarSaveCancelListener = onToolbarSaveCancelListener;
    }

    public void setOnJobDetailToolbarLisener(JobDetailToolbarLisener onJobDetailToolbarLisener) {
        this.mJobDetailToolbarLisener = onJobDetailToolbarLisener;
    }

    public void setOnAlertDetailToolbarLisener(AlertDetailToolbarLisener onAlertDetailToolbarLisener) {
        this.mAlertDetailToolbarLisener = onAlertDetailToolbarLisener;
    }

    public void registerFCM() {
        fcmCurrentToken = FirebaseInstanceId.getInstance().getToken();
        debug(HomeActivity.class, "currentToken: " + fcmCurrentToken);

        fcmId = TokenManager.getInstance().getDeviceId();

        if (fcmCurrentToken != null && !TextUtils.isEmpty(fcmId)
                && !TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""))
                && !TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""))) {

            updateTokenToFirebase();
        }

    }

    private void updateTokenToFirebase() {
        TokenManager.getInstance().validateToken(this, new TokenManager.TokenReponseListener() {
            @Override
            public void onRefreshSuccess() {
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference reference = database.getReference("Users/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "")
                        + "/" + mPrefs.getString(NccConstants.SIGNIN_USER_ID, "") + "/token/" + fcmId);
                TokenModel tokenModel = new TokenModel("Android", fcmCurrentToken, DateTimeUtils.getUtcTimeFormat());
                reference.setValue(tokenModel);
            }

            @Override
            public void onRefreshFailure() {
                tokenRefreshFailed();

            }
        });
    }

    private void showGoOffDutyDialog(boolean mOffDuty) {
        final Dialog dialog = new Dialog(HomeActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_my_job_off_duty);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        TextView mTextMyJobAlert = (TextView) dialog.findViewById(R.id.text_alert_my_job);
        TextView mTextMyJobMessage = (TextView) dialog.findViewById(R.id.text_my_job_message);
        TextView mButtonTextOffDuty = (TextView) dialog.findViewById(R.id.text_button_off_duty);
        TextView mButtonTextMyJobCancel = (TextView) dialog.findViewById(R.id.text_my_job_cancel);
        if (mOffDuty) {
            mTextMyJobAlert.setText(R.string.off_duty_alert_title_success);
            mTextMyJobMessage.setText(R.string.off_duty_alert_message_success);
            mButtonTextOffDuty.setText(R.string.button_text_my_job_success);
            mButtonTextMyJobCancel.setText(R.string.cancel);
        } else {
            if (isUserDispatcher()) {
                mTextMyJobAlert.setText(R.string.off_duty_alert_title_fail);
                mTextMyJobMessage.setText("");
                mButtonTextOffDuty.setText(R.string.off_duty_call_dispatcher);
                mButtonTextOffDuty.setVisibility(View.GONE);
                mButtonTextMyJobCancel.setText(R.string.off_duty_dismiss);
            } else {
                mTextMyJobAlert.setText(R.string.off_duty_alert_title_fail);
                mTextMyJobMessage.setText(R.string.off_duty_alert_message_fail);
                mButtonTextOffDuty.setText(R.string.off_duty_call_dispatcher);
                mButtonTextMyJobCancel.setText(R.string.off_duty_dismiss);
            }

        }

        mButtonTextOffDuty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (Utils.isCallSupported(HomeActivity.this)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (!Dexter.isRequestOngoing()) {
                            Dexter.checkPermission(phoneCallPermissionListener, Manifest.permission.CALL_PHONE);
                        }
                    } else {
                        String phoneNumber = mPrefs.getString(NccConstants.DISPATCHER_NUMBER, "");
                        if (!TextUtils.isEmpty(phoneNumber)) {
                            Utils.makePhoneCall(HomeActivity.this, phoneNumber);
                        }
                    }
                } else {
                    mUserError.message = "Cannot make call";
                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                }
            }
        });
        mButtonTextMyJobCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public boolean isLoggedInDriver() {

        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();

        ArrayList<String> roles = new Gson().fromJson(mPrefs.getString(NccConstants.SIGNIN_USER_ROLE, "[]"), type);

        if (roles != null && roles.size() == 1 && roles.contains("ncc_driver")) {
            return true;
        }

        if (roles != null && (roles.contains(NccConstants.USER_ROLE_DISPATCHER) || roles.contains(NccConstants.USER_ROLE_PARTNER_ADMIN) || roles.contains(NccConstants.USER_ROLE_TEAM_MANAGER))) {
            return false;
        }

        return true; //mPrefs.getString(NccConstants.SIGNIN_USER_ROLE, "").equalsIgnoreCase("Driver");
    }

    public boolean isUserDispatcherAndDriver() {

        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();

        ArrayList<String> roles = new Gson().fromJson(mPrefs.getString(NccConstants.SIGNIN_USER_ROLE, "[]"), type);

        return roles.contains(NccConstants.USER_ROLE_DRIVER) && (roles.contains(NccConstants.USER_ROLE_DISPATCHER) || roles.contains(NccConstants.USER_ROLE_PARTNER_ADMIN));
        //mPrefs.getString(NccConstants.SIGNIN_USER_ROLE, "").equalsIgnoreCase("Driver");
    }

    public boolean isUserDispatcher() {
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();

        ArrayList<String> roles = new Gson().fromJson(mPrefs.getString(NccConstants.SIGNIN_USER_ROLE, "[]"), type);

        return (roles.contains(NccConstants.USER_ROLE_DISPATCHER) || roles.contains(NccConstants.USER_ROLE_PARTNER_ADMIN));
    }

    public boolean isUserDispatcher(List<String> roles) {
        return !(roles.size() == 1 && roles.contains(NccConstants.USER_ROLE_DRIVER));
    }

    public boolean isUserDispatcherAndDriver(List<String> roles) {
        return roles.contains(NccConstants.USER_ROLE_DRIVER) && (roles.contains(NccConstants.USER_ROLE_DISPATCHER) || roles.contains(NccConstants.USER_ROLE_PARTNER_ADMIN));
    }


    public boolean isUserOnlyDriver(List<String> roles) {
        return roles.size() == 1 && roles.contains(NccConstants.USER_ROLE_DRIVER);
    }

    public Observable<Location> getLocationObservable() {
        return mLocationSubject;
    }

    public boolean isOffDuty() {
        if (getResources().getBoolean(R.bool.isTablet)) {
            return mTextDutyLarge.getText().toString().trim().equalsIgnoreCase(getString(R.string.title_offduty));
        } else {
            return mTextDuty.getText().toString().trim().equalsIgnoreCase(getString(R.string.title_offduty));
        }
    }

    @Override
    public void onUpdateNeeded(final String updateUrl) {
        if (!isFinishing()) {
            AlertDialog dialog = new AlertDialog.Builder(this)
                    .setTitle("Update Required")
                    .setMessage("You need to update to the most current version of the app.")
                    .setPositiveButton("Update",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    redirectStore(updateUrl);
                                }
                            }).create();
            dialog.setCancelable(false);
            dialog.show();
        }
    }

    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    public void startSparkLocationService() {
        if (!SparkLocationUpdateService.isRunning(this)) {
            Intent intent = new Intent(this, SparkLocationUpdateService.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(intent);
            } else {
                startService(intent);
            }
        }

    }

    public void stopSparkLocationService() {
        if (SparkLocationUpdateService.isRunning(this)) {
            Timber.d("Stoping the SparklocationService due to onDuty Flag");
            Intent intent = new Intent(this, SparkLocationUpdateService.class);
            stopService(intent);
        }
    }

    public void mintlogEvent(String event) {
        HashMap<String, String> extraData = new HashMap<>();
        if (event.toUpperCase(Locale.ENGLISH).contains("FIREBASE") || event.toUpperCase(Locale.ENGLISH).contains("API")) {
            extraData.put("error", event);
            if (event.toUpperCase(Locale.ENGLISH).contains("FIREBASE")) {
                mintlogEventExtraData("Firebase Exception", extraData);
            } else {
                mintlogEventExtraData("Api Exception", extraData);
            }
        } else {
            mintlogEventExtraData(event, null);
        }

    }

    public void mintlogEventExtraData(String event, HashMap<String, String> extraDatas) {

        Mint.clearExtraData();
        if (extraDatas != null) {
            for (Map.Entry<String, String> extraData :
                    extraDatas.entrySet()) {
                Mint.addExtraData(extraData.getKey(), extraData.getValue());
            }
        }

        if (!TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""))) {
            Mint.setUserIdentifier(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
            Mint.addExtraData("user_id", mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
        }
        if (!TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""))) {
            Mint.addExtraData("facility_id", mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""));
        }

        Mint.logEvent(event, MintLogLevel.Debug);
        Mint.flush();
    }

    public void mintLogException(Exception ex) {
        if (!TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""))) {
            Mint.setUserIdentifier(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
        }
        if (!TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""))) {
            Mint.addExtraData("FacilityId", mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""));
        }
        Mint.logException(ex);
        Mint.flush();
    }

    public void removePushToken() {

        fcmId = TokenManager.getInstance().getDeviceId();

        if (!TextUtils.isEmpty(fcmId)
                && !TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""))
                && !TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""))) {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference reference = database.getReference("Users/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "")
                    + "/" + mPrefs.getString(NccConstants.SIGNIN_USER_ID, "") + "/token");

            reference.child(fcmId).removeValue();
        }
    }

    private void addStatusChangeListener() {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference reference = database.getReference("Users/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "")
                + "/" + mPrefs.getString(NccConstants.SIGNIN_USER_ID, "") + "/status");
        //reference.keepSynced(true);
        TokenManager.getInstance().validateToken(this, new TokenManager.TokenReponseListener() {
            @Override
            public void onRefreshSuccess() {
                reference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String status = dataSnapshot.getValue(String.class);
                        if (!TextUtils.isEmpty(status) && (status.equalsIgnoreCase(NccConstants.USER_STATUS_DELETED) || status.equalsIgnoreCase(NccConstants.USER_STATUS_INACTIVE) || status.equalsIgnoreCase(NccConstants.USER_STATUS_BLOCKED))) {
                            HashMap<String, String> extraDatas = new HashMap<>();
                            extraDatas.put("json", status);
                            mintlogEventExtraData("Home screen User Status", extraDatas);
                            clearSigninData();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (databaseError != null && databaseError.getMessage() != null) {
                            mintlogEvent("Home screen User status Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                        }
                    }
                });
            }

            @Override
            public void onRefreshFailure() {

            }
        });
    }

    private void addRoleChangeListener() {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        Log.i("UserId ", mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
        Log.i("VendorId ", mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""));

        TokenManager.getInstance().validateToken(this, new TokenManager.TokenReponseListener() {
            @Override
            public void onRefreshSuccess() {
                DatabaseReference reference = database.getReference("Users/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "")
                        + "/" + mPrefs.getString(NccConstants.SIGNIN_USER_ID, "") + "/roles");
                //reference.keepSynced(true);
                reference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        ArrayList<String> rolesList = new ArrayList<>();

                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            String role = snapshot.getValue(String.class);
                            rolesList.add(role);
                        }

                        String roles = new Gson().toJson(rolesList);

                        if (rolesList.size() > 0 && !roles.equalsIgnoreCase(mPrefs.getString(NccConstants.SIGNIN_USER_ROLE, "[]"))) {

                            HashMap<String, String> extraDatas = new HashMap<>();
                            extraDatas.put("json", roles);
                            mintlogEventExtraData("User Roles Changed", extraDatas);

                            mEditor.putString(NccConstants.SIGNIN_USER_ROLE, roles).commit();
                            mBottomBar.setCurrentItem(0);
                            mConstraintDuty.setVisibility(View.GONE);
                            hideToolBar();
                            hideBottomBar();
                            checkTermsAndConditions();

                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (databaseError != null && databaseError.getMessage() != null) {
                            mintlogEvent("Home screen User roles changed Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                        }
                    }
                });
            }

            @Override
            public void onRefreshFailure() {

            }
        });
    }

    public void enableSearchListener(SearchView.OnQueryTextListener onQueryTextListener) {

//       if (getResources().getBoolean(R.bool.isTablet)) {
        searchEditTextLarge.setFocusableInTouchMode(true);
        searchEditTextLarge.setFocusable(true);
        searchEditTextLarge.clearFocus();
        searchViewLarge.setOnQueryTextListener(onQueryTextListener);
        searchEditTextLarge.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    searchEditTextLarge.setHint(getString(R.string.formated_zero));
                    searchEditTextLarge.setHintTextColor(getResources().getColor(R.color.search_hint_text_color));
                    if (datePickerLarge.getText().toString().equals(getString(R.string.any_date))) {
                        datePickerLarge.setTextColor(getResources().getColor(R.color.search_hint_text_color));
                    } else {
                        datePickerLarge.setTextColor(getResources().getColor(R.color.search_text_color));
                    }
                } else {
                    searchEditTextLarge.setHint(getString(R.string.any_job_id));
                    searchEditTextLarge.setHintTextColor(getResources().getColor(R.color.search_text_color));
                }
            }
        });


        searchClearLarge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchEditTextLarge.clearFocus();
                searchEditTextLarge.setText("");
                searchEditTextLarge.setHint(getString(R.string.any_job_id));
                searchEditTextLarge.setHintTextColor(getResources().getColor(R.color.search_hint_text_color));
                UiUtils.hideKeyboard(HomeActivity.this);
            }
        });
        clearDateLarge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerLarge.setText(getString(R.string.any_date));
                datePickerLarge.setTextColor(getResources().getColor(R.color.search_hint_text_color));
//                searchEditTextLarge.setText("");
//                searchEditTextLarge.clearFocus();
//                searchEditTextLarge.setHint(getString(R.string.any_job_id));
//                searchEditTextLarge.setHintTextColor(getResources().getColor(R.color.search_text_color));
                clearDateLarge.setVisibility(View.GONE);
                selectedDate = null;
                //dateListener.onDateReset();
                dateListener.onDateSet(null);
            }
        });
        searchSaveLarge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mToolbarSaveListener != null) {
                    mToolbarSaveListener.onSave();
                }
            }
        });
        searchCloseLarge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
//        } else {
        searchEditText.setFocusableInTouchMode(true);
        searchEditText.setFocusable(true);
        searchEditText.clearFocus();
        searchView.setOnQueryTextListener(onQueryTextListener);
        getFilterText();
        searchEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    searchEditText.setHint(getString(R.string.formated_zero));
                    searchEditText.setHintTextColor(getResources().getColor(R.color.ncc_hint_color));
                    if (datePicker.getText().toString().equals(getString(R.string.any_date))) {
                        datePicker.setTextColor(getResources().getColor(R.color.search_hint_text_color));
                    } else {
                        datePicker.setTextColor(getResources().getColor(R.color.search_text_color));
                    }
                } else {
                    searchEditText.setHint(getString(R.string.any_job_id));
                    searchEditText.setHintTextColor(getResources().getColor(R.color.ncc_hint_color));
                }
            }
        });
        searchClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchEditText.clearFocus();
                searchEditText.setText("");
                searchEditText.setHint(getString(R.string.any_job_id));
                searchEditText.setHintTextColor(getResources().getColor(R.color.ncc_hint_color));
                UiUtils.hideKeyboard(HomeActivity.this);
            }
        });

        searchSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mToolbarSaveListener != null) {
                    mToolbarSaveListener.onSave();
                }
            }
        });
        searchClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });

        clearDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePicker.setText(getString(R.string.any_date));
                datePicker.setTextColor(getResources().getColor(R.color.search_hint_text_color));
//                    searchEditText.setText("");
//                    searchEditText.clearFocus();
//                    searchEditText.setHint(getString(R.string.any_job_id));
//                    searchEditText.setHintTextColor(getResources().getColor(R.color.ncc_hint_color));
                clearDate.setVisibility(View.GONE);
                selectedDate = null;
                //dateListener.onDateReset();
                dateListener.onDateSet(null);
            }
        });
//        }
    }

    public void disableSearchListener() {
//        if (getResources().getBoolean(R.bool.isTablet)) {
        if (searchEditTextLarge != null) {
            searchEditTextLarge.setFocusable(false);
            searchEditTextLarge.setFocusableInTouchMode(false);
        }
        if (searchClearLarge != null) {
            searchClearLarge.setOnClickListener(null);
        }
        if (datePickerLarge != null) {
            datePickerLarge.setOnClickListener(null);
        }
        if (clearDateLarge != null) {
            clearDateLarge.setOnClickListener(null);
        }
//        } else {
        if (searchEditText != null) {
            searchEditText.setFocusable(false);
            searchEditText.setFocusableInTouchMode(false);
        }
        if (searchClear != null) {
            searchClear.setOnClickListener(null);
        }
        if (searchView != null) {
            searchView.setOnQueryTextListener(null);
        }
        if (datePicker != null) {
            datePicker.setOnClickListener(null);
        }
        if (clearDate != null) {
            clearDate.setOnClickListener(null);
        }
//        }
    }

    public void clearSearch() {
//        if (getResources().getBoolean(R.bool.isTablet)) {
        if (searchViewLarge != null) {
            searchViewLarge.setQuery("", false);
        }
        searchEditTextLarge.clearFocus();
//        } else {
        if (searchView != null) {
            searchView.setQuery("", false);
        }
        searchEditText.clearFocus();
//        }
    }

    public String getFilterText() {
        String unmaskedFilterText = "";
        String filterText = null;
        if (getResources().getBoolean(R.bool.isTablet)) {
            filterText = searchViewLarge.getQuery().toString();
        } else {
            filterText = searchView.getQuery().toString();
        }
        if (!TextUtils.isEmpty(filterText)) {
            unmaskedFilterText = formatter.formatString(filterText).getUnMaskedString();
        }
        return unmaskedFilterText;
    }

    public void addOnDateSetListener(DateListener dateSetListener) {
        this.dateListener = dateSetListener;
        datePickerLarge.setEnabled(true);
        datePicker.setEnabled(true);
//        if (getResources().getBoolean(R.bool.isTablet)) {
        datePickerLarge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });
//        } else {
        datePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });
//        }
    }

    public void clearDate() {
        selectedDate = null;
//        if (getResources().getBoolean(R.bool.isTablet)) {
        datePickerLarge.setText(getString(R.string.any_date));
        datePickerLarge.setTextColor(getResources().getColor(R.color.search_hint_text_color));
        clearDateLarge.setVisibility(View.GONE);
//        } else {
        datePicker.setText(getString(R.string.any_date));
        datePicker.setTextColor(getResources().getColor(R.color.search_hint_text_color));
        clearDate.setVisibility(View.GONE);
//        }
    }

    public void setSearchText(String text, String date) {
        if (datePickerLarge != null && !TextUtils.isEmpty(date)) {
            datePickerLarge.setText(date);
            datePickerLarge.setTextColor(getResources().getColor(R.color.ncc_blue));
        }
        if (datePicker != null && !TextUtils.isEmpty(date)) {
            datePicker.setText(date);
            datePicker.setTextColor(getResources().getColor(R.color.ncc_blue));
        }

        if (searchViewLarge != null) {
            searchViewLarge.setQuery(text, false);
        }

        if (searchView != null) {
            searchView.setQuery(text, false);
        }

    }


    public Calendar getSelectedDate() {
        return selectedDate;
    }

    public void submitSearch() {
        String text = searchView.getQuery().toString();
//        if (!TextUtils.isEmpty(text)) {
        searchView.setQuery(text, true);
//        }
        if (getSelectedDate() != null && dateListener != null) {
            dateListener.onDateSet(getSelectedDate().getTime());
        }
    }

    public void resetJobId() {
        mJobId = "";
    }

    private LocationRequest mLocationRequest;
    private static final int REQUEST_CHECK_SETTINGS = 455;

    protected LocationRequest getLocationRequest() {
        if (mLocationRequest == null) {
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(10000);
            mLocationRequest.setFastestInterval(5000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        }
        return mLocationRequest;
    }

    /**
     * to request device location settings on
     */
    public void checkLocationSettings() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(getLocationRequest());
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                // All location settings are satisfied. The client can initialize
                // location requests here.
                getLocationUpdates();
            }
        });

        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(HomeActivity.this,
                                REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException sendEx) {
                        // Ignore the error.
                    }
                }
            }
        });
    }

    PermissionListener locPermissionListener = new PermissionListener() {
        @Override
        public void onPermissionGranted(PermissionGrantedResponse response) {
            requestLocationUpdates();
        }

        @Override
        public void onPermissionDenied(PermissionDeniedResponse response) {

        }

        @Override
        public void onPermissionRationaleShouldBeShown(PermissionRequest permission,
                                                       PermissionToken token) {
            token.continuePermissionRequest();
        }
    };

    Handler mHandler = new Handler();

    LocationCallback mLocationCallback = new LocationCallback() {

        String deviceId = TokenManager.getInstance().getDeviceId();

        @Override
        public void onLocationResult(LocationResult locationResult) {
            if (locationResult == null) {
                return;
            }
            for (Location location : locationResult.getLocations()) {

                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        SparkLocationData locationData = new SparkLocationData(
                                location.getTime(),
                                location.getLatitude(),
                                location.getLongitude(),
                                location.getAltitude(),
                                0,
                                location.getAccuracy(),
                                location.getSpeed(),
                                0.0,
                                "Android",
                                deviceId
                        );

                        if (myRef != null) {
                            myRef.child("location").setValue(locationData);
                        }
                        if (myLocationRef != null) {
                            myLocationRef.push().setValue(locationData);
                        }
                    }
                }, 500);
            }
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHECK_SETTINGS && resultCode == RESULT_OK) {
            getLocationUpdates();
        }
    }

    public void getLocationUpdates() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                requestLocationUpdates();
            } else {
                if (!Dexter.isRequestOngoing() && !isFinishing()) {
                    Dexter.checkPermission(locPermissionListener, Manifest.permission.ACCESS_FINE_LOCATION);
                }
            }
        } else {
            requestLocationUpdates();
        }
    }

    private void requestLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.requestLocationUpdates(getLocationRequest(), mLocationCallback, null);
    }

    public void stopLocationUpdates() {
        if (mFusedLocationClient != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
    }

    public interface OnDutyToolbarListener {
        void onDutyCall();

        void onTextDuty();

        void offDutyCall();
    }

    public interface ToolbarSaveListener {
        void onSave();
    }

    public interface ToolbarSaveCancelListener {
        void onSave();

        void onCancel();
    }

    public interface JobDetailToolbarLisener {
        void onCall();

        void onNotes();

        void onOptions();
    }


    public interface AlertDetailToolbarLisener {
        void onSettings();
    }

    public interface DutyUpdateListener {
        void onSuccess();

        void onFailure();
    }

    private void addIgnoringBatteryOptimizationsCallback() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            NccSdk.getNccSdk(getApplication()).setIgnoringBatteryOptimizationsCallback(new NccSdkInterface.IgnoringBatteryOptimizationsCallback() {
                @Override
                public void onDisabled() {
                    mintlogEvent("IgnoreBatteryOptimizations is disabled");
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopLocationUpdates();
    }
}