package com.agero.ncc.adapter;


import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.model.Profile;
import com.agero.ncc.utils.NccConstants;
import com.agero.nccsdk.NccSdk;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class AssignDriverListAdapter extends RecyclerView.Adapter<AssignDriverListAdapter.ItemViewHolder> {

    @Inject
    public SharedPreferences mPrefs;
    public SharedPreferences.Editor mEditor;
    private Context mContext;
    private ArrayList<Profile> mAssignDriverList;
    private HashMap<String, Integer> mAssignedJobsCount;
    String mFLname = "";
    int mLastClickedEquipment;
    JobDetail mCurrentJob;

    public AssignDriverListAdapter(Context mContext, ArrayList<Profile> mAssignDriverList, HashMap<String, Integer> mAssignedJobsCount, int mLastClickedEquipment, JobDetail currentJob) {
        this.mContext = mContext;
        this.mAssignDriverList = mAssignDriverList;
        this.mAssignedJobsCount = mAssignedJobsCount;
        this.mLastClickedEquipment = mLastClickedEquipment;
        NCCApplication.getContext().getComponent().inject(this);
        mEditor = mPrefs.edit();
        mCurrentJob = currentJob;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.assign_driver_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {

        if (mAssignDriverList.get(position).getImageUrl() != null) {
            Glide.with(mContext).load(mAssignDriverList.get(position).getImageUrl()).into(holder.mImagePhoto);
            holder.mTextLableFlname.setVisibility(View.GONE);
        } else {
            holder.mTextLableFlname.setVisibility(View.VISIBLE);
            mFLname = mAssignDriverList.get(position).getFirstName().charAt(0) + "" + mAssignDriverList.get(position).getLastName().charAt(0);
            holder.mTextLableFlname.setText(mFLname);
        }
        holder.mTextDriverName.setText(mAssignDriverList.get(position).getFirstName() + " " + mAssignDriverList.get(position).getLastName());
        if (mAssignDriverList.get(position).getOnDuty() != null && mAssignDriverList.get(position).getOnDuty()) {
            holder.mTextDriverEquipment.setTextColor(mContext.getResources().getColor(R.color.ncc_black));
            if (mAssignDriverList.get(position).getEquipment() != null && mAssignDriverList.get(position).getEquipment().getEquipmentType() != null) {
                holder.mTextDriverEquipment.setText(mAssignDriverList.get(position).getEquipment().getEquipmentType());
            }else {
                holder.mTextDriverEquipment.setText(mContext.getResources().getString(R.string.toolbar_noequipment));
            }
        } else {
            holder.mTextDriverEquipment.setTextColor(mContext.getResources().getColor(R.color.assign_driver_jobassign_color));
            holder.mTextDriverEquipment.setText("Off Duty");
            holder.mTextDriverAssigned.setVisibility(View.INVISIBLE);
        }
        if (mLastClickedEquipment == position) {
            holder.mRadioButtonAssignDriver.setChecked(true);
        } else {
            holder.mRadioButtonAssignDriver.setChecked(false);
        }

        if(mAssignedJobsCount != null && mAssignedJobsCount.size() > 0) {
            //Number of  Job Assigned For Indiviual Driver
            if (mAssignedJobsCount.containsKey(mAssignDriverList.get(position).getUserId())) {
                int jobCount = mAssignedJobsCount.get(mAssignDriverList.get(position).getUserId());
                holder.mTextDriverAssigned.setText(jobCount + " jobs assigned");
            } else {
                holder.mTextDriverAssigned.setText("0 jobs assigned");
            }
        }else{
            holder.mTextDriverAssigned.setText("-");
        }
        if (mAssignDriverList.get(position).getLocation() != null && mAssignDriverList.get(position).getLocation().getLatitude() != null && mAssignDriverList.get(position).getLocation().getLongitude() != null && mCurrentJob.getDisablementLocation() != null && mCurrentJob.getDisablementLocation().getLatitude() != null && mCurrentJob.getDisablementLocation().getLongitude() != null) {
            android.location.Location mDisablementLocation = new android.location.Location("");
            android.location.Location mDriverlocation = new android.location.Location("");
//            try {
//                if(NCCApplication.mLocationData != null){
//                    mCurrentlocation.setLatitude(NCCApplication.mLocationData.getLatitude());
//                    mCurrentlocation.setLongitude(NCCApplication.mLocationData.getLongitude());
//                } else{
//                    mCurrentlocation.setLatitude(0.0);
//                    mCurrentlocation.setLongitude(0.0);
//                }
//
//            } catch (Exception e) {
//                mCurrentlocation.setLatitude(0.0);
//                mCurrentlocation.setLongitude(0.0);
//            }

            mDisablementLocation.setLatitude(mCurrentJob.getDisablementLocation().getLatitude());
            mDisablementLocation.setLongitude(mCurrentJob.getDisablementLocation().getLongitude());

            mDriverlocation.setLatitude(mAssignDriverList.get(position).getLocation().getLatitude());
            mDriverlocation.setLongitude(mAssignDriverList.get(position).getLocation().getLongitude());

            holder.mTextDriverAssigned.setText(holder.mTextDriverAssigned.getText().toString() + " " +
                    mContext.getString(R.string.dot) + " " + String.format("%.2f", getMiles(mDriverlocation.distanceTo(mDisablementLocation))) + " mi away");
        }


    }

    private double getMiles(float meters) {
        if(meters != 0f) {
            return meters / 1609.344;
        }else {
            return 0f;
        }
    }

    @Override
    public int getItemCount() {
        return mAssignDriverList.size();
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.radioButton_assign_driver)
        RadioButton mRadioButtonAssignDriver;
        @BindView(R.id.image_photo)
        CircleImageView mImagePhoto;
        @BindView(R.id.text_lable_flname)
        TextView mTextLableFlname;
        @BindView(R.id.text_driver_name)
        TextView mTextDriverName;
        @BindView(R.id.text_driver_equipment)
        TextView mTextDriverEquipment;
        @BindView(R.id.text_driver_assigned)
        TextView mTextDriverAssigned;
        @BindView(R.id.view_border_assigned)
        View mViewBorderAssigned;

        ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
