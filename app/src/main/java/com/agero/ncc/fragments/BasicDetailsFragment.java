package com.agero.ncc.fragments;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.model.Profile;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BasicDetailsFragment extends CameraBaseFragment {
    HomeActivity mHomeActivity;
    @BindView(R.id.image_profile_photo)
    ImageView mImageProfilePhoto;
    @BindView(R.id.image_button_camera)
    ImageButton mImageButtonCamera;
    Profile mProfile;
    @BindView(R.id.text_firstname)
    TextView mTextFirstname;
    @BindView(R.id.text_mobilenumber)
    TextView mTextMobilenumber;
    @BindView(R.id.text_email)
    TextView mTextEmail;
    @BindView(R.id.text_password)
    TextView mTextPassword;
    protected UserError mUserError;
    private long oldRetryTime = 0;
    private DatabaseReference myRef;
    private ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            loadProfile(dataSnapshot);
            hideProgress();
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if(isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Basic Details User Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getProfileDataFromFirebase();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };

    public BasicDetailsFragment() {
        // Intentionally empty
    }

    public static BasicDetailsFragment newInstance() {
        BasicDetailsFragment fragment = new BasicDetailsFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_basic, fragment_content, true);
        ButterKnife.bind(this, view);
        NCCApplication.getContext().getComponent().inject(this);
        mEditor = mPrefs.edit();
        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.hideBottomBar();
        mHomeActivity.hideToolBar();
        if (!mPrefs.getString(NccConstants.CAMERA_IMAGE_PREF_KEY, "").isEmpty()) {
            mImageProfilePhoto.setImageURI(Uri.parse(mPrefs.getString(NccConstants.CAMERA_IMAGE_PREF_KEY, "")));
        }
        mUserError = new UserError();
        getProfileDataFromFirebase();
        return view;
    }

    private void getProfileDataFromFirebase() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                         myRef = database.getReference("Users/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "")
                                + "/" + mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                        //myRef.keepSynced(true);
                        myRef.addValueEventListener(valueEventListener);
                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    if(isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

    private void loadProfile(DataSnapshot dataSnapshot) {
        if (dataSnapshot.hasChildren()) {
            try {
                mProfile = dataSnapshot.getValue(Profile.class);

                if(mProfile != null) {
                    HashMap<String, String> extraDatas = new HashMap<>();
                    extraDatas.put("json", new Gson().toJson(mProfile));
                    mHomeActivity.mintlogEventExtraData("Basic Details User Profile", extraDatas);
                }
                mTextFirstname.setText(mProfile.getFirstName() + " " + mProfile.getLastName());
                mTextEmail.setText(mProfile.getEmail());
                if (!mProfile.getPassword().isEmpty() && mProfile.getPassword() != null) {
                    mTextPassword.setText("Change");
                }
                PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                try {
                    Phonenumber.PhoneNumber formatedNumber = phoneUtil.parse(mProfile.getMobilePhoneNumber(), "US");
                    mTextMobilenumber.setText(phoneUtil.format(formatedNumber, PhoneNumberUtil.PhoneNumberFormat.NATIONAL));
                } catch (NumberParseException e) {
                    Log.e("BaseDetailsFragment", "NumberParseException was thrown: " + e.toString());
                }
            }catch (Exception e){
               mHomeActivity.mintLogException(e);
            }
        }
    }

    @OnClick({R.id.image_profile_photo, R.id.image_button_camera})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_profile_photo:
                break;
            case R.id.image_button_camera:
                showPhotoSelectionBottomSheet(SHOW_BOTTOM_SHEET_BASIC_DETAILS);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                mEditor.putString(NccConstants.CAMERA_IMAGE_PREF_KEY, onSelectFromGalleryResult(data)).commit();
                mImageProfilePhoto.setImageURI(Uri.parse(onSelectFromGalleryResult(data)));
            } else if (requestCode == REQUEST_CAMERA) {
                mEditor.putString(NccConstants.CAMERA_IMAGE_PREF_KEY, onCaptureImageResult(data).toString()).commit();
                mImageProfilePhoto.setImageURI(onCaptureImageResult(data));
            }

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(myRef != null) {
            myRef.removeEventListener(valueEventListener);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if(myRef != null) {
            myRef.removeEventListener(valueEventListener);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(myRef != null) {
            myRef.removeEventListener(valueEventListener);
        }
    }

}
