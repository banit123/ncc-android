package com.agero.ncc.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.agero.ncc.R;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.views.SignatureView;
import com.agero.ncc.views.VerticalTextView;
import com.bumptech.glide.Glide;

import java.io.ByteArrayOutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * To capture customer signature.
 */
public class SignatureFragment extends BaseFragment implements SignatureView.SignTouchLisener {
    @BindView(R.id.frame_signature)
    RelativeLayout mRelativeLayout;
    @BindView(R.id.text_done_signature)
    VerticalTextView mTextDoneSignature;
    @BindView(R.id.constraint_done_signature)
    ConstraintLayout mConstraintDoneSignature;
    @BindView(R.id.image_get_signature)
    ImageView mImageGetSignature;
    private SignatureView mSignatureView;
    private Bitmap mSignatureBitmap;
    HomeActivity mHomeActivity;
    boolean isFromDisablementSign;
    String mImageUrl;


    public SignatureFragment() {
        // Intentionally empty
    }

    public static SignatureFragment newInstance(boolean isFromDisablementSign, String mImageurl) {
        SignatureFragment fragment = new SignatureFragment();
        Bundle args = new Bundle();
        args.putBoolean(NccConstants.IS_IT_FROM_DISABLEMENT_SIGN, isFromDisablementSign);
        args.putString(NccConstants.IMAGE_URL, mImageurl);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_signature, fragment_content, true);
        ButterKnife.bind(this, view);
        mHomeActivity = ((HomeActivity) getActivity());
        mHomeActivity.hideBottomBar();
        mHomeActivity.hideToolBar();
        if (getArguments() != null) {
            isFromDisablementSign = getArguments().getBoolean(NccConstants.IS_IT_FROM_DISABLEMENT_SIGN);
            mImageUrl = getArguments().getString(NccConstants.IMAGE_URL);
        }
        if (mImageUrl.isEmpty()) {
            //        add custom signature view to view group
            mImageGetSignature.setVisibility(View.GONE);
            signatureView();

            //        startActivity(new Intent(getActivity(), SignatureNavigationActivity.class));
        } else {
            mImageGetSignature.setVisibility(View.VISIBLE);
            mRelativeLayout.setEnabled(false);
            Glide.with(getContext())
                    .load(mImageUrl)
                    .into(mImageGetSignature);
        }
        mTextDoneSignature.setEnabled(false);
        return superView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mHomeActivity = ((HomeActivity) getActivity());
        mHomeActivity.hideBottomBar();
        mHomeActivity.hideToolBar();
        mHomeActivity.hideDuty();
    }

    private void signatureView() {
        mSignatureView = new SignatureView(getActivity());
        mSignatureView.setOnSignTouchLisener(this);
        mRelativeLayout.addView(mSignatureView, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mRelativeLayout.setDrawingCacheEnabled(true);
    }

    @OnClick({R.id.image_close, R.id.text_done_signature, R.id.text_clear_signature})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.image_close:
                getActivity().onBackPressed();
                break;
            case R.id.text_done_signature:
                if (mSignatureView.isSigned()) {
                    generateSignatureImage();
                } else {
                    showMessage(getString(R.string.please_sign));
                }
                break;
            case R.id.text_clear_signature:
                mImageGetSignature.setVisibility(View.GONE);
                if(mSignatureView!=null && mSignatureView.isSigned()){
                    mSignatureView.undo();
                }
                signatureView();
                mTextDoneSignature.setEnabled(false);
                mRelativeLayout.setEnabled(true);
                mConstraintDoneSignature.setBackground(getResources().getDrawable(R.drawable.border_signature_grey));
                mTextDoneSignature.setTextColor(getResources().getColor(R.color.edit_text_color));
                break;
        }
    }

    /**
     * To create bitmap of the signature.
     */
    private void generateSignatureImage() {
        if (mRelativeLayout.getDrawingCache() != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            mRelativeLayout.getDrawingCache().compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            mSignatureBitmap = rotateBitmap(BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length), -90);
            if (isFromDisablementSign) {
                SignatureDetailsFragment.mDisablementBitmap = mSignatureBitmap;
            } else {
                SignatureDetailsFragment.mTowBitmap = mSignatureBitmap;
            }
            getActivity().onBackPressed();
        }
    }

    /**
     * To rotate bitmap since the layout is in landscape mode.
     *
     * @param source bitmap
     * @param angle  angle
     * @return rotated bitmap
     */
    private Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }


    @Override
    public void onSignTouchLisener() {
        mConstraintDoneSignature.setBackgroundColor(getResources().getColor(R.color.ncc_background_button));
        mTextDoneSignature.setTextColor(getResources().getColor(R.color.ncc_white));
        mTextDoneSignature.setEnabled(true);
    }


}
