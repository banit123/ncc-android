package com.agero.ncc.utils;


public class UserError {

    public String errorId;
    public CharSequence title;
    public CharSequence message;

    public UserError() {
    }

    public UserError(String errorId, CharSequence title, CharSequence message) {
        this.errorId = errorId;
        this.title = title;
        this.message = message;
    }

    @Override
    public String toString() {
        return "UserError{" +
                "errorId='" + errorId + '\'' +
                ", title=" + title +
                ", message=" + message +
                '}';
    }
}
