package com.agero.ncc.views;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.Guideline;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.utils.NccConstants;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TowBottomSheetDialog extends BottomSheetDialogFragment {
    HomeActivity mHomeActivity;
    @BindView(R.id.guideline_service)
    Guideline guidelineService;
    @BindView(R.id.text_tow_destination)
    TextView mTextTowDestination;
    @BindView(R.id.image_get_directions)
    ImageView mImageGetDirections;
    @BindView(R.id.text_get_directions)
    TextView mTextGetDirections;
    @BindView(R.id.image_edit_tow_destination)
    ImageView mImageEditTowDestination;
    @BindView(R.id.text_edit_tow_destination)
    TextView mTextEditTowDestination;
    boolean isTow;
    JobDetail mCurrentJob;

    TowBottomClickLisener towBottomClickLisener;


    public static TowBottomSheetDialog newInstance(boolean isTow_, String jobDetail) {
        TowBottomSheetDialog fragment = new TowBottomSheetDialog();
        Bundle args = new Bundle();
        args.putBoolean(NccConstants.EDIT_TOW_DESTINATION, isTow_);
        args.putString(NccConstants.TOW_JOB_DETAIL, jobDetail);
        fragment.setArguments(args);
        return fragment;
    }

    public void setTowBottomClickLisener(TowBottomClickLisener towBottomClickLisener) {
        this.towBottomClickLisener = towBottomClickLisener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tow_bottom_sheet, container, false);
        ButterKnife.bind(this, view);
        mHomeActivity = (HomeActivity) getActivity();

        if (getArguments() != null) {
            isTow = getArguments().getBoolean(NccConstants.EDIT_TOW_DESTINATION);
            mCurrentJob = new Gson().fromJson(getArguments().getString(NccConstants.TOW_JOB_DETAIL), JobDetail.class);
        }
        if (isTow) {
            mTextTowDestination.setText(getResources().getString(R.string.edit_tow_destination_label));
            mTextEditTowDestination.setText(getResources().getString(R.string.edit_tow_destination_edit));
        } else {
            mTextTowDestination.setText(getResources().getString(R.string.edit_disabalement_location_label));
            mTextEditTowDestination.setText(getResources().getString(R.string.edit_disablement_location));
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                BottomSheetDialog dialog = (BottomSheetDialog) getDialog();
                FrameLayout bottomSheet = (FrameLayout)
                        dialog.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                behavior.setPeekHeight(0);
            }
        });
    }

    @OnClick({R.id.image_get_directions, R.id.text_get_directions, R.id.image_edit_tow_destination, R.id.text_edit_tow_destination})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_get_directions:
                towBottomClickLisener.isGetDirectionClick(isTow);
                break;
            case R.id.text_get_directions:
                towBottomClickLisener.isGetDirectionClick(isTow);
                break;
            case R.id.image_edit_tow_destination:
                towBottomClickLisener.isTowDisablemtClick(isTow);
            case R.id.text_edit_tow_destination:
                dismiss();
                towBottomClickLisener.isTowDisablemtClick(isTow);
                break;
        }
    }


    public interface TowBottomClickLisener {
        void isGetDirectionClick(boolean isTowDirection);

        void isTowDisablemtClick(boolean isTow);
    }
}
